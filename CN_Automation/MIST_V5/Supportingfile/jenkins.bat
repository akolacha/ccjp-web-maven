cd\
cls
echo %~n0
set Batfilepath="%~dp0
set suite=%~1
set env=%~2

call python "%Batfilepath%UpdateConfigFiles_JP.py"

if "%suite%"== "Skim" (
  if "%env%"== "SQA" (
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Sanity_EXECUTE_THIS.bat
  )
)

if "%suite%"== "Skim" (
  if "%env%"== "UAT" (
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Sanity_EXECUTE_THIS_UAT.bat
  )
)

if "%suite%"== "Regression" (
  if "%env%"== "SQA" (
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_Regresison_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_Regresison_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_Regresison_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Regresison_EXECUTE_THIS.bat

  )
)

if "%suite%"== "Regression" (
  if "%env%"== "UAT" (
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_Regresison_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_Regresison_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_Regresison_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Regresison_EXECUTE_THIS_UAT.bat
  )
)

if "%suite%"== "ALL" (
  if "%env%"== "SQA" (
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Sanity_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_Regresison_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_Regresison_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_Regresison_EXECUTE_THIS.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Regresison_EXECUTE_THIS.bat
  )
)

if "%suite%"== "ALL" (
  if "%env%"== "UAT" (
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Sanity_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_Lion\\"CN_Lion_Regresison_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_tsuruha\\"CN_tsuruha_Regresison_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JPW\\"CN_JPW_Regresison_EXECUTE_THIS_UAT.bat
	call %Batfilepath%"\\CN_JP\\"CN_JP_Regresison_EXECUTE_THIS_UAT.bat

  )
)
exit
