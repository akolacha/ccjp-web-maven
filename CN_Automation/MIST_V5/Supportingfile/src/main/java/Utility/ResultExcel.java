package Utility;
/*
 ********************************************************************************
 ********************************************************************************
 
 //  Start date 7 Jan 2014
 //  Author : Santhosha HC
 //  Test :   Result excel 

 ********************************************************************************
 ********************************************************************************/
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import static Utility.R_Start.ResPathFolderOuter;
import static Utility.R_Start.Resultdata;
import static Utility.R_Start.driver;
import static Utility.R_Start.StoreTable;
import static gtaf.controller.Controller.ModuleDriver;
public class ResultExcel
{
  public static int rownum = 0;
  public static int SummarySheetrownum = 0;
  public static int TestIDCol = 0;
  public static int ModulenameCol = 1;
  public static int RequirementsCol = 2;
  public static int TesdDescCol = 3;
  public static int KeywordCol = 4;
  public static int PassDescCol = 5;
  public static int FailDescCol = 6;
  public static int ExpectedCol = 7;
  public static int ActualCol = 8;
  public static int StatusCol = 9;
  public static int ScreenshotCol = 10;
  public static int SLNOCol = 0;
  public static int MODULENAMECol = 1;
  public static int NoofTestCasesPassedCol = 2;
  public static int NoofTestCasesFailedCol = 3;
  public static int ExecutionTimeCol = 4;
  public static int DefectIDCol = 5;
  public static int NotesCol = 6;
  public static XSSFWorkbook WorkBook;
  public static XSSFSheet ResultSheet;
  public static FileOutputStream out;
  public static XSSFCell cell;
  public static CellStyle style;
  public static File ResPathFolderDate;
  public static File ResPathFolderDateBrowserName;
  public static String ResultExlFilepath;
  public static String ResImageFolderPath;
  public static String ResPathFolderPdfPath;
  public static String  CurrentFilepath = null;
  public static String  ALMCurrentFilepath = null;
  public static Date date;
  public static String CaptureScreenCol  = "ScreenShot" ;
  
  public static void FW_CreateResultFile()
    throws IOException
  {
    WorkBook = new XSSFWorkbook();
    SummarySheetrownum = 0;
    
    ResultSheet = WorkBook.createSheet("Summary");
    
    Row row = ResultSheet.createRow(SummarySheetrownum++);
    ResultSheet.setDefaultColumnWidth(30);
    


    
    Cell cell = row.createCell(1);
    cell.setCellValue("Executed on "+ StoreTable.get("Execute ON Browser") +" Browser ");
    
    
    setCellColorAndFontColorBlue((XSSFCell)cell);
    
    cell = row.createCell(2);
    cell.setCellValue("Test Summary");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    
    cell = row.createCell(3);
    String BuildVersion = (String)R_Start.StoreTable.get("Build Version_Description");
    cell.setCellValue(BuildVersion);
    setCellColorAndFontColorBlue((XSSFCell)cell);
    
    row = ResultSheet.createRow(SummarySheetrownum++);
    

    cell = row.createCell(SLNOCol);
    cell.setCellValue("SL NO");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    
    cell = row.createCell(MODULENAMECol);
    cell.setCellValue("MODULE NAME");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    

    cell = row.createCell(NoofTestCasesPassedCol);
    cell.setCellValue("No of Test Cases Passed");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    

    cell = row.createCell(NoofTestCasesFailedCol);
    cell.setCellValue("No of Test Cases Failed");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    
    cell = row.createCell(ExecutionTimeCol);
    cell.setCellValue("Execution Time in Sec");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    

    cell = row.createCell(DefectIDCol);
    cell.setCellValue("Defect ID");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    

    cell = row.createCell(NotesCol);
    cell.setCellValue("Notes");
    setCellColorAndFontColorBlue((XSSFCell)cell);
    



    date = new Date();
    

    String WorkningDir = System.getProperty("user.dir");
    
    String[] A = WorkningDir.split("\\\\");
    
    int Strlen = A.length;
    String StrB = "";
    for (int i = 0; i < Strlen - 1; i++) {
      StrB = StrB + "\\" + A[i];
    }
    SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
    String formattedDate = sdf.format(date);
    
    
	 ResPathFolderDate =  new File( ResPathFolderOuter.getAbsolutePath()+"\\Result_"+ ModuleDriver +"_" +formattedDate );
	 ResPathFolderDate.mkdir();
	
	 
	 File ResPathFolderExcel =  new File( ResPathFolderDate.getAbsolutePath()+"\\ExcelResults" );
	 ResPathFolderExcel.mkdir();
	
	 
	 File ResPathFolderPdf =  new File( ResPathFolderDate.getAbsolutePath()+"\\PdfReports" );
	 ResPathFolderPdf.mkdir();
	 ResPathFolderPdfPath = ResPathFolderPdf.getAbsolutePath();
	 
	 
	File ResImageFolder =new File( ResPathFolderDate.getAbsolutePath()+"\\Screen_Shots");
	ResImageFolder.mkdir();
	ResImageFolderPath = ResImageFolder.getAbsolutePath();
	
    ResultExlFilepath = ResPathFolderExcel.getAbsolutePath()+"\\Result_" + TC.ProjectName+ formattedDate +"_"+ StoreTable.get("Execute ON Browser") + ".xlsx";
    
    
    
    out = new FileOutputStream(new File(ResultExlFilepath));
    WorkBook.write(out);
    
    out.close();
    System.out.println("Done");
  }
  
  public static void setCellColorAndFontColor(XSSFCell cell)
  {
    XSSFFont font = WorkBook.createFont();
    style = WorkBook.createCellStyle();
    
    font.setBold(true);
    font.setColor(IndexedColors.BLACK.getIndex());
    style.setBorderBottom((short)1);
    style.setBorderLeft((short)1);
    style.setBorderRight((short)1);
    style.setBorderTop((short)1);
    style.setFont(font);
    style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
    style.setFillPattern((short)1);
    cell.setCellStyle(style);
  }
  
  public static void setCellColorAndFontColorBlue(XSSFCell cell)
  {
    XSSFFont font = WorkBook.createFont();
    style = WorkBook.createCellStyle();
    
    font.setBold(true);
    font.setColor(IndexedColors.WHITE.getIndex());
    style.setBorderBottom((short)1);
    style.setBorderLeft((short)1);
    style.setBorderRight((short)1);
    style.setBorderTop((short)1);
    style.setFont(font);
    style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
    style.setFillPattern((short)1);
    cell.setCellStyle(style);
  }
  
  public static void FW_CreateModuleResultFile()
    throws IOException
  {
    Logs.Ulog("Start  FW_CreateModuleResultFile into result ");
    rownum = 0;
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, TestIDCol, rownum, "TestID");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, ModulenameCol, rownum, "ModuleName");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, RequirementsCol, rownum, "Requirements");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, KeywordCol, rownum, "Keyword");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, TesdDescCol, rownum, "TestDescription");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, PassDescCol, rownum, "PassDescription");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, FailDescCol, rownum, "FailDescription");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, ExpectedCol, rownum, "Expected");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, ActualCol, rownum, "Actual");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, StatusCol, rownum, "Status");
    R_Start.Resultdata.ReslutSetCellData(TC.TestModuleName, ScreenshotCol, rownum, "ScreenShot");
    R_Start.Resultdata.ReslutSetRowColor(TC.TestModuleName, 0, rownum, "yellow");
    rownum += 1;
    rownum += 1;
    Logs.Ulog("End  FW_CreateModuleResultFile into result ");
  }
  
  
	
}

