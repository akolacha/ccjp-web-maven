package Utility;
import java.io.File;
import java.io.IOException;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.*;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import br.eti.kinoshita.testlinkjavaapi.model.ReportTCResultResponse;

import java.net.MalformedURLException;
import java.net.URL;






//here
import br.eti.kinoshita.testlinkjavaapi.constants.ActionOnDuplicate;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.constants.ResponseDetails;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseStepAction;
import br.eti.kinoshita.testlinkjavaapi.constants.TestImportance;
import br.eti.kinoshita.testlinkjavaapi.model.Attachment;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.CustomField;
import br.eti.kinoshita.testlinkjavaapi.model.Execution;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import br.eti.kinoshita.testlinkjavaapi.model.ReportTCResultResponse;
import br.eti.kinoshita.testlinkjavaapi.model.Requirement;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestCaseStep;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestProject;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;
import br.eti.kinoshita.testlinkjavaapi.model.User;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

/*
 ********************************************************************************
 ********************************************************************************

 //  Start date 7 Jan 2014
 //  Author : Santhosha HC
 //  Test :    Driver script V 1.9 

 ********************************************************************************
 ********************************************************************************/
import static Utility.FunctionLibrary.driver;
import static Utility.ResultExcel.FW_CreateModuleResultFile;
import static Utility.R_Start.myRest;
import static Utility.R_Start.StoreTable;
import static Utility.HTMLResults.DetailedReportFilePath;
import static Utility.HTMLResults.DetailedReportPDFFilePath;
import static Utility.HTMLResults.AllTestFolderPath;
import static Utility.HTMLResults.PDfdocument;
import static Utility.HTMLResults.PdfPagecontent;
import static Utility.ResultExcel.ResPathFolderPdfPath;
import io.appium.java_client.service.local.AppiumDriverLocalService;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import static Utility.HTMLResults.MsgBody;
import static Utility.HTMLResults.S_no;
import net.sourceforge.htmlunit.corejs.javascript.ast.WhileLoop;
import net.sourceforge.htmlunit.corejs.javascript.tools.shell.Environment;
import op2Utilities.TargetProcess;

import org.apache.commons.io.DirectoryWalker.CancelException;
import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.SeleniumServer;
import org.sikuli.script.Screen;

import static Utility.GmailSendMail.ModuleSenMailFilepath;
import static Utility.ALM.ALMFilepath;
import OR_TestParm.OR;
import Utility.UserdefinedLibrary;
import autoitx4java.AutoItX;

import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.jacob.com.LibraryLoader;
import com.jayway.restassured.RestAssured;
import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import static Utility.TestNgReportsForDashboard.TestNgSteps;
import static gtaf.controller.Controller.ModuleDriver;
import static gtaf.controller.Controller.ModuleRow;
import static gtaf.controller.Controller.ModuleRowCount;
import static gtaf.controller.Controller.Pbar;
import static gtaf.controller.Controller.TestLinkBuildID;
import static gtaf.controller.Controller.TestLinkDevKey;
import static gtaf.controller.Controller.TestLinkPlanID;
import static gtaf.controller.Controller.TestLinkPlatform;
import static gtaf.controller.Controller.TestLinkProjectID;
import static gtaf.controller.Controller.TestLinkServerURL;
import static gtaf.controller.Controller.TestlinkTesterAssigned;

//  Start date 7 Jan 2014
//  Author : Santhosh  H C
public class R_Start extends FunctionLibrary {
	public R_Start( String ModuleDriver) throws InterruptedException, IOException, ClassNotFoundException {
		super();
		
		ReuseTableData = new Hashtable();
		StoreTable = new Hashtable();
		
		Properties prop = new Properties();
		InputStream input = null;	 
		
		File file = new File(System.getProperty("user.dir"));
		parentFolder = file.getParentFile();
		try
		{
			if(System.getProperty("excelFilePath")!=null){
				String filePath = System.getProperty("excelFilePath");
				input = new FileInputStream(filePath+"/Supportingfile/src/main/java/Config.properties");
			}
				else
				{
					input = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/Config.properties");
				}
			
			
		}
		catch(Throwable t)
		{
			//Logs.Ulog("Please check Config.Properties file present in src  folder" +System.getProperty("user.dir")+ "/Config.Properties");
			Logs.Ulog(t.getMessage());			
		}
		prop.load(input);
		try
		{
			
			CreateTestfiles();
		}
		catch(Throwable t)
		{
			Logs.Ulog("Please Makesure Resource.xlsm present in Parent folder folder" + parentFolder);
			Logs.Ulog("Please Makesure Module_*.xlsm present in Script folder" + parentFolder);
			Logs.Ulog(t.getMessage());
			
		}
		LoadObjects();
		loadJacobLibrary();
		Setup.LoadSetup();
		Setup.LoadWindowsObjects();
		Setup.LoadGlobalSetup();
		ReusableFunctions.LoadReuseData();
		ReusableFunctions.LoadTestData();
		

	}
	public static File parentFolder;
	public static Logger TTlogs;
	public static AutoItX Window;
	public static AppiumDriverLocalService Appiumserver = null;
	public static SeleniumServer SeleniumLocalServer;
	public static  Map<String, String> MyMethods;
	public static Screen WinScreen;
	public static ExcelObj Testdata;
	public static ExcelObj SetupData;
	public static ExcelObj GlobalSetupData;
	public static ExcelObj Resourcedata;
	public static ExcelObj Resultdata;
	public static String ExecutionModUleName = "TestScripts.";
	public static int DriverRow = 2;
	public static int Testrow = 2;
	public static int TestObjRow = 1;
	public static int TestRowCount;
	public static Date date;
	public static String PrvKeyword;
	public static String StrLine = "##############################################################################################################";
	public static Hashtable ORTable;
	public static Hashtable StoreTable;
	public static Hashtable DeviceTable;
	public static Hashtable ReuseTableData;
	public static WebElement TObj;
	public static WebElement FailTObj;
	public static ResultExcel ResFile;
	public static String Status;
	public static String ConditionStatus;
	public static int Steptimedelay;
	public static long startTime;
	public static long endTime;
	public static long totalTime;
	public static long FinaltotalTime;
	public static long ModuleFinaltotalTime;
	public static   Capabilities cap; 
	public static Selenium S_Obj;
	public static String  ScriptStartTime;
	public static String  ScriptEndTime;
	public static String   FinalScriptStartTime;
	public static String   ModuleFinalScriptStartTime;
	public static File ResPathFolderOuter;
	public static int DriverRowCount ;
	public static RestAssured myRest;
	public static WsdlProject mySoapProj;
	public static WsdlInterface mywsdl;
	public static SimpleDateFormat sdf ;
	public static String AllPassMsgBody;
	public static String  AllPassFilepath; 
	public static String HeaderAllPassMsgBody;
	public static String TngScriptEndTime;
	public static String TngScriptStartTime;
	public static String TngFinalScriptStartTime;
	public static SimpleDateFormat   DATE_FORMAT;
	public static String Browser_Chrome ;
	public static String Browser_IE ;
	public static String Browser_Mozilla;
	public static String Browser_Safari ;
	public static String ALMTESTID ;
	public static String PrintLogs = "YY" ;
	public static String GenerateLogs = "YY" ;
	
	
	/*public static void main(String[] args) throws InterruptedException,
			ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, IOException, CancelException {

		ResultExcel ResFile = new ResultExcel();

		R_Start RS = new R_Start(ConditionStatus);
		RS.initialize();
		Logs.Ulog("End of Main Script");

	}*/

	
	
public static void loadJacobLibrary() throws ClassNotFoundException {
		
		try
		{
			
		
		String jvmBitVersion = System.getProperty("sun.arch.data.model");
		System.out.println("JVM Bit Version : " + jvmBitVersion);
		if (jvmBitVersion.contains("32")) {
			System.setProperty("jacob.dll.path", System.getProperty("user.dir") + "/ReferenceJars/jacob-1.18-x86.dll");
		}
		else {
			System.setProperty("jacob.dll.path", System.getProperty("user.dir") + "/ReferenceJars/jacob-1.18-x64.dll");
		}	
		}
		catch( Throwable T)
		{
			Logs.Ulog("Error While Loading Jacob Dll" + T.getMessage());
		}
		
			
		try
		{
		LibraryLoader.loadJacobLibrary();
		}
		catch( Throwable T)
		{
			Logs.Ulog("Error While Loading Jacob loadJacobLibrary" + T.getMessage());
		}
		
		try
		{
			Window  =  new AutoItX();
		}
		catch( Throwable T)
		{
			Logs.Ulog("Error While creating Autoit WindowObject" + T.getMessage());
		}
		try
		{
			Window  =  new AutoItX();
		}
		catch( Throwable T)
		{
			Logs.Ulog("Error While creating Autoit WindowObject" + T.getMessage());
		}
		
		try
		{
		 
		  MyMethods =  new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
		Class c = Class.forName("Utility.R_Start");
		for (Method MYmethod : c.getMethods())
		{
			//System.out.println( MYmethod.getName().toString());
			MyMethods.put(MYmethod.getName().toString(), MYmethod.getName().toString());			
			
		}
		
		/*for (String name:   MyMethods.keySet()){String key =name.toString();
            String value = MyMethods.get(name).toString();System.out.println(key );}  */
		}
		catch( Throwable T)
		{
			Logs.Ulog("Error While creating Autoit WindowObject" + T.getMessage());
		}
		
		
		Logs.Ulog("Well loaded RS" );
}
	 public void initialize() throws InterruptedException,ClassNotFoundException, NoSuchMethodException, SecurityException,	IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, IOException, CancelException {
		
		// ******************************************************************************
		   
		    sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
		    date = new Date();
		    
		    DATE_FORMAT = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a'Z'");
			 TngFinalScriptStartTime  = DATE_FORMAT.format(date);
			 
		    FinalScriptStartTime = sdf.format(date);
		    HTMLResults Rhtml = new HTMLResults();
		    Rhtml.FWGenerateMainReport();
		    GmailSendMail Sm = new  GmailSendMail();
			

		// ******************************************************************************
		

		Steptimedelay = 0;
		if (!StoreTable.get("Execution Delay in Each Step").equals(""))
			Steptimedelay = (int) Double.parseDouble((String) StoreTable
					.get("Execution Delay in Each Step"));

		Logs.Ulog("Excels initialized");
		DriverRowCount = Testdata.getRowCount(TC.DriverSheet);
		Logs.Ulog("Driver Rows Count = " + DriverRowCount);
		
		
		
		for (DriverRow = 2; DriverRow <= DriverRowCount; DriverRow++) {
			
			
			try
			{
				TC.TestModuleName = Testdata.getCellData(TC.DriverSheet,
						TC.TestScenarioNameCol, DriverRow);
				if(TC.TestModuleName.isEmpty())
				{
					Logs.Ulog("Error - Please update /change 'TestModuleName' column name in driver sheet to  'TestScenarioName' Or no Module is set to 'YY' " );
				    TC.TestModuleName = Testdata.getCellData(TC.DriverSheet,
						TC.TestModuleNameCol, DriverRow);
				}
			}
			catch( Throwable T)
			{
				Logs.Ulog("Error - Please update /change 'TestModuleName' column name in driver sheet to  'TestScenarioName' " + T.getMessage());
				TC.TestModuleName = Testdata.getCellData(TC.DriverSheet,
						TC.TestModuleNameCol, DriverRow);
			}
			//Added by Avanish for testlink integration
			
			TC.flagUpdateresultTotestLink= Testdata.getCellData(TC.DriverSheet,
					TC.flagUpdateresultTotestLinkCol, DriverRow);
			TC.TestLinkTCID= Testdata.getCellData(TC.DriverSheet,
					TC.TestLinkTCIDCol, DriverRow);
			TC.TestLinkTCVersion= Testdata.getCellData(TC.DriverSheet,
					TC.TestLinkTCVersionCol, DriverRow);
			//till here
			//Added by Avanish on 13-1-17
			TC.TestLinkExternalID= Testdata.getCellData(TC.DriverSheet,
					TC.TestLinkExternalIDCol, DriverRow);
			//Added till here by Avanish
			TC.ExecuteModule = Testdata.getCellData(TC.DriverSheet,
					TC.ExecuteModuleCol, DriverRow);
			TC.ExecuteModule = TC.ExecuteModule.toUpperCase();
			Logs.Ulog(" *********** Executing Driver ************  "
					+ TC.TestModuleName + " **** Execute =  "
					+ TC.ExecuteModule);
			
			TC.Dependency = Testdata.getCellData(TC.DriverSheet,
					TC.DependencyModuleCol, DriverRow);
			TC.AlmTestID = Testdata.getCellData(TC.DriverSheet,
					TC.AlmTestIDCol, DriverRow);
			if (TC.ExecuteModule.equals(TC.ExecuteModuleYes)) {
				
				
				
				if (TC.Dependency.equalsIgnoreCase(TC.ExecuteTestYes))
				{
					
					if (UpdateResult.FinalStatus.equalsIgnoreCase("PASS") )
					{
						//********************************
							date = new Date();
							 DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
							 TngScriptStartTime  = DATE_FORMAT.format(date);
							
							startTime = System.currentTimeMillis();
							ScriptStartTime = sdf.format(date);
							
			
							Resultdata = new ExcelObj(ResultExcel.ResultExlFilepath);
							
							// ************************************************************************************
							date = new Date();
						    sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
				            String formattedDate = sdf.format(date);  	                      
							
							   DetailedReportFilePath  = AllTestFolderPath + "\\"+TC.TestModuleName+ formattedDate +".Html";
							   DetailedReportPDFFilePath  = ResPathFolderPdfPath + "\\"+TC.TestModuleName+ formattedDate +".pdf";  //Updated by S a n t h o s h a H C Jan 2016 ALM integration
							   try
								{
								 if (StoreTable.get("ALM_UpdateStatus").toString().equalsIgnoreCase("YY"))
									{
									 if (StoreTable.get("ALM_UpdateStatus_HTMLAttchment").toString().equalsIgnoreCase("YY")) 
									   {
										   ALM_UpdateStatus_HTMLAttchment = "YY";
									   }
									   if (StoreTable.get("ALM_UpdateStatus_XLSAttchment").toString().equalsIgnoreCase("YY")) 
									   {
										   ALM_UpdateStatus_XLSAttchment = "YY";
									   }
									   new  ALM().ALMRunAppender();

										  Process p = Runtime.getRuntime().exec("C:/windows/SysWOW64/cscript.exe " + ALMFilepath);
									
									} 
								}
								catch(Throwable t)
								{
									Logs.Ulog("Error Alm cobnfigutration failures please update with proper alm configurations");
								}
								
							   File DetailedReportFile = new File( DetailedReportFilePath ); 
							   DetailedReportFile.createNewFile();
							   HTMLResults.HTMLDetailedReportInitialize();
							// ResultExcel.FW_CreateResultFile();
							   //Updated by S a n t h o s h a H C Jan 2016 ALM integration
			
							// ************************************************************************************
							 if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
							{
							Resultdata.addSheet(TC.TestModuleName);
							UpdateResult.ResultSheet = TC.TestModuleName;
							ResultExcel.FW_CreateModuleResultFile();
							}
							// ************************************************************************************
			
							 Pbar.Driverprogress(ModuleRow , "In Module " + ModuleRow + " of " + ModuleRowCount + " ==> " + ModuleDriver + " ==> @ Test# " + DriverRow + " of "
										+ DriverRowCount + " ==> "
										+ TC.TestModuleName );
							 PrograssBar.TestProgressBar.setForeground(Color.GREEN);
							System.out.println("Executing Driver  " + TC.TestModuleName);
			
							TestRowCount = Testdata.getRowCount(TC.TestModuleName);
							Logs.Ulog("Test Rows Count = " + TestRowCount);
							for (Testrow = 2; Testrow <= TestRowCount; Testrow++) {
								TC.LoadTestData(Testrow);
			//					if (!TC.Module.equals("") && TC.TCID.equals(""))
								if (!TC.Module.equals(""))
									UpdateResult.UpDateModule();
			
								
								Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
										+ "=== Execute =  " + TC.ExecuteTest);
								// System.out.println(" *********** Executing Keywords  ************  "
								// + TC.Keyword);
								if (TC.PauseExecution.equals("YY"))
								{
									 while (TC.PauseExecution.equals("YY")) {
										 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
								            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
								           Pbar.p.setBackground(Color.YELLOW);
								            TimeUnit.SECONDS.sleep(3);
								           Pbar.p.setBackground(Color.WHITE);
								        }
									 
									 TC.PauseExecution = "NN";
									 Pbar.p.setBackground(new Color(189,183,107));
								}
								if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
									Logs.Ulog("=== Keywords === " + TC.Keyword
											+ "=== Execute =  " + TC.ExecuteTest);
									Logs.Ulog("Executing " + TC.TestObjects + "."
											+ TC.Keyword + " " + TC.InputData + " @ Row  "
											+ Testrow + " of " + TestRowCount);
									
									Pbar.Testprogress(Testrow, PrvKeyword, "@ Step   " + Testrow + " of "
											+ TestRowCount + "  "
											+ TC.TestObjects + "." + TC.Keyword + " "
											+ TC.InputData );
			
									UpdateResult.UpDateKeyword();
									PrvKeyword = TC.Keyword;
									if (TC.Keyword.startsWith("IF_ObjCondition") ) {
										Logs.Ulog("=== is a IF IF_ObjCondition =======");
										ConStates.IF_ObjCondition();
									}
									else if (TC.Keyword.startsWith("IF_VarCondition") ) {
										Logs.Ulog("=== is a IF IF_VarCondition =======");
										ConStates.IF_VarCondition();
									}
									else if (TC.Keyword.startsWith("ForLoop")) {
										Logs.Ulog("=== is a Reuse keyword =======");
										
										ConStates.ForLoop();
										
									}
									else if (TC.Keyword.startsWith("ReuseForLoop")) {
										Logs.Ulog("=== is a Reuse keyword =======");
										
										ConStates.ReuseForLoop();
										
									}
									else if (TC.Keyword.startsWith("ReUse_")) {
										Logs.Ulog("=== is a Reuse keyword =======");
										ReusableFunctions
												.ExeCurrentReusbleFunction(TC.Keyword);
									}
									
									else {
										try {
											if (!TC.Keyword.startsWith("IF_") && !TC.Keyword.equalsIgnoreCase("End_IF") && !TC.Keyword.equalsIgnoreCase("End_ForLoop") )
											{
													Logs.Ulog("=== Starting Keywords === "
															+ TC.Keyword + "=== Execute =  "
															+ TC.ExecuteTest);
													// Class c =
													// Class.forName("FunctionLibrary.UtilityFunction");
													Class c = Class.forName("Utility.R_Start");
					//								String TPAck_Class = ExecutionModUleName
					//										+ TC.TestModuleName;
					//								Class c = Class.forName(TPAck_Class);
													
													
														String KW = TC.Keyword;
														try
														{
															TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
															if (TC.Keyword.equals(null))
															{
																 TC.Keyword =  KW;
															}
														}
														 catch(Throwable t)
														 {
															 TC.Keyword =  KW;
														 }
													 
													 if(TC.Keyword.equalsIgnoreCase("wait"))
															TC.Keyword = "Wait";
													  Method m = c.getMethod(TC.Keyword, null);
																									
												
													// m.invoke(null, null);
													Status = (String) m.invoke(null, null);
													
													Logs.Ulog("=== End of Keywords === "
															+ TC.Keyword + "=== Execute =  "
															+ TC.ExecuteTest);
					
													TimeUnit.SECONDS.sleep(Steptimedelay);
													Logs.Ulog("=== Execution Delay === "
															+ Steptimedelay);
													long Et = (System.currentTimeMillis() - startTime) / 1000;
													Logs.Ulog("------- EXECTION TIME TAKEN--------  = "
															+ String.valueOf(Et));
													StepSync();
											}
			
										} catch (Exception e) {
											Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
													+ TC.Keyword
													+ " === Result State  "
													+ PrvKeyword);
											Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
											UpdateResult.ActualData = TC.FAIL;
											TC.FailDescription =" ===  Main Sheet Wrong KEYWORD Unable to find the keyword Check the keyword in keyword column === ";
											UpdateResult.UpdateStatus();
										}
			
										
									}// End of keyword else
			
								} // End of Test Execution if
								
								if(!(Status==null))
								{
									if (Status.equals("FAIL") && TC.OnFailExit.equalsIgnoreCase(TC.StopTestYes) )
									{						
										Logs.Ulog(" === Test Failed on  === "+ TC.Keyword + " === Result State " + PrvKeyword);
											
										 if (StoreTable.get("CloseBrowserOnExitFailure").toString().equalsIgnoreCase("YY"))
											{
												try
												{
													//driver.close();
													driver.quit();
												}
												catch(Throwable t)
												{
													Logs.Ulog("ERROR ------ Error While Closing browser/Devices " + t.getMessage());
												}
											
											}
										break ;
									}
									
								}
							}
							date = new Date();
							 DATE_FORMAT = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a'Z'");
							TngScriptEndTime  = DATE_FORMAT.format(date);
							sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
						   formattedDate = sdf.format(date);
						    ScriptEndTime = formattedDate;
						    
							endTime = System.currentTimeMillis();
							totalTime = ((endTime - startTime) / 1000);
							//Up date XMl report for dash board
							TestNgReportsForDashboard.updateTestNG_XmlFIleTestStatus();
							TestNgReportsForDashboard.StoreTestNgSteps();
							TestNgReportsForDashboard.updateTestNG_XmlEndTags();
							
							UpdateResult.UpDateSummary();
							FinaltotalTime = FinaltotalTime + totalTime;
							ModuleFinaltotalTime = ModuleFinaltotalTime + totalTime;
							
							UpdateResult.TestCasePassed = 0;
							UpdateResult.TestCaseFailed = 0;
							totalTime = 0;
							try
							{
								PdfPagecontent.close();	
								PDfdocument.save(DetailedReportPDFFilePath);
							    PDfdocument.close();
							}
							catch(Throwable T)
							{
								Logs.Ulog("Error while saving pdf my result repot file" + T.getMessage());
								try
								{
									
								    PDfdocument.close();
								}
								catch(Throwable t)
								{
									Logs.Ulog("Error while saving pdf my result repot file" + t.getMessage());
								}
								
							}
							try
							{
							 if (StoreTable.get("ALM_UpdateStatus").toString().equalsIgnoreCase("YY"))
								{
								 if (StoreTable.get("ALM_UpdateStatus_HTMLAttchment").toString().equalsIgnoreCase("YY")) 
								   {
									   ALM_UpdateStatus_HTMLAttchment = "YY";
								   }
								   if (StoreTable.get("ALM_UpdateStatus_XLSAttchment").toString().equalsIgnoreCase("YY")) 
								   {
									   ALM_UpdateStatus_XLSAttchment = "YY";
								   }
								   if (StoreTable.get("ALM_UpdateStatus_PDFAttchment").toString().equalsIgnoreCase("YY")) 
								   {
									   ALM_UpdateStatus_PDFAttchment = "YY";
								   }
								   new  ALM().ALMRunAppender();

									  Process p = Runtime.getRuntime().exec("C:/windows/SysWOW64/cscript.exe " + ALMFilepath);
								
								} 
							}
							catch(Throwable t)
							{
								Logs.Ulog("Error Alm cobnfigutration failures please update with proper alm configurations");
							}
							
							
							UpdateResult.GlobalStatus = "PASS";
					}//End of Final status if
					else
					{
						TC.SkipTestCase = true;
						HTMLResults.UpdateMainReport();
						TC.SkipTestCase =false;
					}
				 } // End Dependency if
				
				else
				{
					TestCaseExecution();
				}
			}// End of Driver Execution if
				
				
			Logs.Ulog(StrLine);
			 if (StoreTable.get("CloseBrowserEndOfEachTest").toString().equalsIgnoreCase("YY"))
				{
					try
					{
						//driver.close();
						driver.quit();
					}
					catch(Throwable t)
					{
						Logs.Ulog("ERROR ------ Error While Closing browser/Devices " + t.getMessage());
					}
				
				}
			
		}// End of driver execution for
		UpdateResult.UpDateTotalSummary();
		UpdateResult.SLNO=1;		
		UpdateResult.TestCasePassed  = 0 ;
		UpdateResult.TestCaseFailed  = 0 ;
		UpdateResult.TotalTestCasePassed  = 0 ;
		UpdateResult.TotalTestCaseFailed  = 0 ;
		UpdateResult.ModuleTestCasePassed =0;
		UpdateResult.ModuleTestCaseFailed =0;
		
		FinaltotalTime = 0;
		S_no =0;
		
		Logs.Ulog("End of Script");
		
		//Update testng xml report for dash board
		//TestNgReportsForDashboard.updateTestNG_XmlFIleModuleStatus();
		
		
		
		Resultdata = null;
		Pbar.Driverprogress(100,
				"Test Suite Execution completed " + date.toString());
		Pbar.Testprogress(100, PrvKeyword, "Current test execution completed "
				+ date.toString());
		Logs.Ulog("End of Progress");
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
	    String formattedDate = sdf.format(date);
	    String  EmailFolderPath = System.getProperty("java.io.tmpdir")+"ModuleSendEmail";
		String  Filepath =  EmailFolderPath+ "/"+ModuleDriver + formattedDate+"_"+ UpdateResult.FinalStatus +  ".vbs";
		
		File ResPathFolder =  new File( EmailFolderPath );
		ResPathFolder.mkdir();
		
	
		
		if(StoreTable.get("SendMailOutLook").toString().equalsIgnoreCase("YY"))
		{
			Sm.WriteEmail(MsgBody , Filepath  , ModuleDriver  , UpdateResult.FinalStatus );
		    Sm.WriteEmailOutLooKEmail(MsgBody , Filepath  , ModuleDriver  , UpdateResult.FinalStatus );
		  
		}
		if(StoreTable.get("SendMailByModuleOutLook").toString().equalsIgnoreCase("YY"))
		{
			
		    Sm.WriteEmailOutLookWithoutPort(MsgBody , Filepath  , ModuleDriver  , UpdateResult.FinalStatus );
		    Process p = Runtime.getRuntime().exec("cmd.exe /C start " +Filepath);
		  
		}
		

		
		
		
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
		//update the  pass report email'
		
		if (UpdateResult.FinalStatus.equalsIgnoreCase("PASS"))
		{
			
			HeaderAllPassMsgBody = HeaderAllPassMsgBody + AllPassMsgBody +"</table>";
			
		  String  AllPassFilepath =  EmailFolderPath+"/AllPass_Test_"+ UpdateResult.FinalStatus +  ".vbs";
		  Sm.WriteEmail(HeaderAllPassMsgBody , AllPassFilepath  , "All Modules"  , UpdateResult.FinalStatus );
		 // Sm.WriteEmailOutLooKEmail(HeaderAllPassMsgBody , AllPassFilepath  , "All Modules"  , UpdateResult.FinalStatus );
		  
		  
       }			 
		
		if(StoreTable.get("SendMailByAllPassModule").toString().equalsIgnoreCase("YY"))
		{
			
		  Process p = Runtime.getRuntime().exec("cmd.exe /C start " + AllPassFilepath);
		}
		
	
		File file = new File(OR.Testdatatemp);
		file.delete();
		
		
		
	}

///Test case execution block

public static void TestCaseExecution() throws IOException, InterruptedException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, CancelException
{
		
	boolean testLinkUpdated=false;
	date = new Date();
	startTime = System.currentTimeMillis();
	ScriptStartTime = sdf.format(date);
	 DATE_FORMAT = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a'Z'");
	 TngScriptStartTime  = DATE_FORMAT.format(date);

	Resultdata = new ExcelObj(ResultExcel.ResultExlFilepath);
	// ************************************************************************************
	date = new Date();
    sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
    String formattedDate = sdf.format(date);  	                      
    
	 
	   DetailedReportFilePath  = AllTestFolderPath + "\\"+TC.TestModuleName+ formattedDate +".Html";
	   DetailedReportPDFFilePath  = ResPathFolderPdfPath + "\\"+TC.TestModuleName+ formattedDate +".pdf";
	   File DetailedReportFile = new File( DetailedReportFilePath); 
	   DetailedReportFile.createNewFile();
	   HTMLResults.HTMLDetailedReportInitialize();
	   
	
	// ResultExcel.FW_CreateResultFile();

	// ************************************************************************************
	 if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
	{
	Resultdata.addSheet(TC.TestModuleName);
	UpdateResult.ResultSheet = TC.TestModuleName;
	ResultExcel.FW_CreateModuleResultFile();
	}
	// ************************************************************************************

	 Pbar.Driverprogress(ModuleRow , "In Module " + ModuleRow + " of " + ModuleRowCount + " ==> " + ModuleDriver + " ==> @ Test# " + DriverRow + " of "
				+ DriverRowCount + " ==> "
				+ TC.TestModuleName );
	 PrograssBar.TestProgressBar.setForeground(Color.GREEN);
	System.out.println("Executing Driver  " + TC.TestModuleName);

	TestRowCount = Testdata.getRowCount(TC.TestModuleName);
	Logs.Ulog("Test Rows Count = " + TestRowCount);
	for (Testrow = 2; Testrow <= TestRowCount; Testrow++) {
		TC.LoadTestData(Testrow);
//					if (!TC.Module.equals("") && TC.TCID.equals(""))
		if (!TC.Module.equals(""))
			UpdateResult.UpDateModule();

		
		Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
				+ "=== Execute =  " + TC.ExecuteTest);
		// System.out.println(" *********** Executing Keywords  ************  "
		// + TC.Keyword);
		
		if (TC.PauseExecution.equals("YY"))
		{
			 while (TC.PauseExecution.equals("YY")) {
				 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
		            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
		           Pbar.p.setBackground(Color.YELLOW);
		            TimeUnit.SECONDS.sleep(3);
		           Pbar.p.setBackground(Color.WHITE);
		        }
			 
			 TC.PauseExecution = "NN";
			 Pbar.p.setBackground(new Color(189,183,107));
		}
		if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
			Logs.Ulog("=== Keywords === " + TC.Keyword
					+ "=== Execute =  " + TC.ExecuteTest);
			Logs.Ulog("Executing " + TC.TestObjects + "."
					+ TC.Keyword + " " + TC.InputData + " @ Row  "
					+ Testrow + " of " + TestRowCount);
			Pbar.Testprogress(Testrow, PrvKeyword, "@ Step   " + Testrow + " of "
					+ TestRowCount +"  "
					+ TC.TestObjects + "." + TC.Keyword + " "
					+ TC.InputData );

			UpdateResult.UpDateKeyword();
			PrvKeyword = TC.Keyword;
			if (TC.Keyword.startsWith("IF_ObjCondition") ) {
				Logs.Ulog("=== is a IF IF_ObjCondition =======");
				ConStates.IF_ObjCondition();
				if(Status.equals(TC.BLOCK))
					break;
			}
			else if (TC.Keyword.startsWith("IF_VarCondition") ) {
				Logs.Ulog("=== is a IF IF_VarCondition =======");
				ConStates.IF_VarCondition();
				if(Status.equals(TC.BLOCK))
					break;
			}
			else if (TC.Keyword.startsWith("ForLoop")) {
				Logs.Ulog("=== is a Reuse keyword =======");
				
				ConStates.ForLoop();
				if(Status.equals(TC.BLOCK))
					break;
				
			}
			else if (TC.Keyword.startsWith("ReuseForLoop")) {
				Logs.Ulog("=== is a Reuse keyword =======");
				
				ConStates.ReuseForLoop();
				if(Status.equals(TC.BLOCK))
					break;
				
			}
			else if (TC.Keyword.startsWith("ReUse_")) {
				Logs.Ulog("=== is a Reuse keyword =======");
				ReusableFunctions
						.ExeCurrentReusbleFunction(TC.Keyword);
				if(Status.equals(TC.BLOCK))
					break;
				
			}
			
			else {
				try {
					if (!TC.Keyword.startsWith("IF_") && !TC.Keyword.equalsIgnoreCase("End_IF") && !TC.Keyword.equalsIgnoreCase("End_ForLoop") )
					{
							Logs.Ulog("=== Starting Keywords === "
									+ TC.Keyword + "=== Execute =  "
									+ TC.ExecuteTest);
							// Class c =
							// Class.forName("FunctionLibrary.UtilityFunction");
							Class c = Class.forName("Utility.R_Start");
//								String TPAck_Class = ExecutionModUleName
//										+ TC.TestModuleName;
//								Class c = Class.forName(TPAck_Class);
							String KW = TC.Keyword;
							try
							{
								TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
								
								if (TC.Keyword.equals(null))
								{
									 TC.Keyword =  KW;
								}
							}
							 catch(Throwable t)
							 {
								 TC.Keyword =  KW;
							 }
							 
							 
							 if(TC.Keyword.equalsIgnoreCase("wait"))
									TC.Keyword = "Wait";
							Method m = c.getMethod(TC.Keyword, null);
							// m.invoke(null, null);
							Status = (String) m.invoke(null, null);
												
//added by Avanish to update testlink block status
							if(Status.equals(TC.BLOCK)){
								m=c.getMethod("CloseBrowser", null);
								m.invoke(null, null);
								Testrow = TestRowCount+1;
								break;
							}
							TC.stepCtr++;
							if(Status.equalsIgnoreCase(TC.FAIL)|| Status.equalsIgnoreCase("false")){
								TC.failStepCtr++;
							}	
							
//till here added
//##################################################							
							//updateTCResult(updateTCResult(int testCaseID, int testPlanID, String status, int buildNumber,String notes, String platformName,String Server_URL, String DevKey); )
							Logs.Ulog("=== End of Keywords === "
									+ TC.Keyword + "=== Execute =  "
									+ TC.ExecuteTest);

							TimeUnit.SECONDS.sleep(Steptimedelay);
							Logs.Ulog("=== Execution Delay === "
									+ Steptimedelay);
							long Et = (System.currentTimeMillis() - startTime) / 1000;
							Logs.Ulog("------- EXECTION TIME TAKEN--------  = "
									+ String.valueOf(Et));
							StepSync();
					}

				} catch (Exception e) {
					Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
							+ TC.Keyword
							+ " === Result State  "
							+ PrvKeyword);
					Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
					UpdateResult.ActualData = TC.FAIL;
					TC.FailDescription =" ===  Main Sheet Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === ";
					UpdateResult.UpdateStatus();
				}

				
			}// End of keyword else

		} // End of Test Execution if
		
		if(!(Status==null))
		{
			if (Status.equals("FAIL") && TC.OnFailExit.equalsIgnoreCase(TC.StopTestYes) )
			{						
				Logs.Ulog(" === Test Failed on  === "+ TC.Keyword + " === Result State " + PrvKeyword);
				
				 if (StoreTable.get("CloseBrowserOnExitFailure").toString().equalsIgnoreCase("YY"))
					{
						try
						{
							//driver.close();
							driver.quit();
						}
						catch(Throwable t)
						{
							Logs.Ulog("ERROR ------ Error While Closing browser/Devices " + t.getMessage());
						}
					
					}
				
				break ;
			}
			
		}
	}
	date = new Date();
	sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
   formattedDate = sdf.format(date);
    ScriptEndTime = formattedDate;
    DATE_FORMAT = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
	TngScriptEndTime  = DATE_FORMAT.format(date);
	endTime = System.currentTimeMillis();
	totalTime = ((endTime - startTime) / 1000);
	UpdateResult.UpDateSummary();
	//Up date XMl report for dash board
	TestNgReportsForDashboard.updateTestNG_XmlFIleTestStatus();
	TestNgReportsForDashboard.StoreTestNgSteps();
	TestNgReportsForDashboard.updateTestNG_XmlEndTags();
	FinaltotalTime = FinaltotalTime + totalTime;
	ModuleFinaltotalTime = ModuleFinaltotalTime + totalTime;
	
	UpdateResult.TestCasePassed = 0;
	UpdateResult.TestCaseFailed = 0;
	totalTime = 0;
	try
	{
		PdfPagecontent.close();	
		PDfdocument.save(DetailedReportPDFFilePath);
	    PDfdocument.close();
	}
	catch(Throwable T)
	{
		Logs.Ulog("Error while saving pdf my result repot file" + T.getMessage());
		try
		{
			
		    PDfdocument.close();
		}
		catch(Throwable t)
		{
			Logs.Ulog("Error while saving pdf my result repot file" + t.getMessage());
		}
		
	}
	
	try
	{
	 if (StoreTable.get("ALM_UpdateStatus").toString().equalsIgnoreCase("YY"))
		{
		 if (StoreTable.get("ALM_UpdateStatus_HTMLAttchment").toString().equalsIgnoreCase("YY")) 
		   {
			   ALM_UpdateStatus_HTMLAttchment = "YY";
		   }
		   if (StoreTable.get("ALM_UpdateStatus_XLSAttchment").toString().equalsIgnoreCase("YY")) 
		   {
			   ALM_UpdateStatus_XLSAttchment = "YY";
		   }
		   
		   if (StoreTable.get("ALM_UpdateStatus_PDFAttchment").toString().equalsIgnoreCase("YY")) 
		   {
			   ALM_UpdateStatus_PDFAttchment = "YY";
		   }
		   new  ALM().ALMRunAppender();

			  Process p = Runtime.getRuntime().exec("C:/windows/SysWOW64/cscript.exe " + ALMFilepath);
		
		} 
	}
	catch(Throwable t)
	{
		Logs.Ulog("Error Alm cobnfigutration failures please update with proper alm configurations");
	}
	//Updated by Avanish for testlink integration
		if(TC.TestLinkStatus!=TC.BLOCK){
		TC.TestLinkStatus=UpdateResult.GlobalStatus;
		}
		System.out.println("TestLinkID->"+TC.TestLinkTCID);
		System.out.println("TestExtrnlID->"+TC.TestLinkExternalID);
		System.out.println("TestLinkStatus->"+TC.TestLinkStatus);
		//updated 30/12
		System.out.println("TestLink Platform->"+TestLinkPlatform);
//updated by Avanish for testlink Block Status		
		/*TC.testFailPercent=(TC.failStepCtr/(double)TC.stepCtr)*100;
		if(TC.testFailPercent>=60){
			TC.TestLinkStatus=TC.BLOCK;
		}*/
	
		if(TC.flagUpdateresultTotestLink.trim().equalsIgnoreCase("YY")){
			for(int i=0;i<TC.TestLinkTCID.split(",").length;i++){
			//addTestCaseToTestPlan(TestLinkProjectID,TestLinkPlanID,TC.TestLinkTCID.split(",")[i], TC.TestLinkTCVersion, TestLinkPlatform);
			//assignTestCaseToTester(TC.TestLinkTCID.split(",")[i], TestLinkPlanID, TestlinkTesterAssigned, TestLinkBuildID,TestLinkPlatform);
			//testLinkUpdated=updateTeslinkResult(TC.TestLinkTCID.split(",")[i],TestLinkPlanID,TC.TestLinkStatus,TestLinkBuildID,TestLinkPlatform);
				testLinkUpdated=updateTeslinkResult(TC.TestLinkTCID.split(",")[i],TestLinkPlanID,TC.TestLinkStatus,TestLinkBuildID,TestLinkPlatform);
			}
		}
		Status = UserdefinedLibrary.deleteuser_ui();
		//KillProcess();
	//updated till here
	
	UpdateResult.GlobalStatus = "PASS";
	TC.failStepCtr=0;
	TC.stepCtr=0;
	TC.TestLinkStatus=TC.PASS;
	
	
	
//End of Final status if


}
//Method added by Avanish for testlink integration
public static boolean updateTeslinkResult(String testCaseID, String testPlanID, String status, String buildNumber,String platformName){
	int tcase_ID;
	int tplan_ID;
	int bldNo;
	try{
		tcase_ID=(int)Double.parseDouble(testCaseID.trim());
		tplan_ID=(int)Double.parseDouble(testPlanID);
		bldNo=(int)Double.parseDouble(buildNumber);
		if(TC.TestLinkStatus.equalsIgnoreCase(TC.PASS)||TC.TestLinkStatus.equalsIgnoreCase(TC.FAIL)||TC.TestLinkStatus.equalsIgnoreCase(TC.BLOCK)){
			//updateTCResult(tcase_ID,tplan_ID,TC.TestLinkStatus,bldNo,"",platformName,"","");
			TargetProcess TP= new TargetProcess();
			TP.bulkUpdate(bldNo, Arrays.asList(tcase_ID), status+"ED", "");
			return true;
		
		}
	}catch(NumberFormatException e){
		Logs.Ulog("Error while parsing the parameters for testlink" + e.getMessage());
		return false;
	} catch (Exception e) {
		Logs.Ulog("Error while calling testlink API" + e.getMessage());
		return false;
		
	} 
	Logs.Ulog("Error while updating the status to TestLink, Testlink status found to update in testlink is:"+status);
	return false;
}
//Method added by Avanish for testlink integration
public static void updateTCResult(int testCaseID, int testPlanID, String status, int buildNumber,String notes, String platformName,String Server_URL, String DevKey) throws TestLinkAPIException, MalformedURLException{
	DevKey=TestLinkDevKey;	
	Server_URL=TestLinkServerURL;
	URL testlinkURL = new URL(Server_URL);
			
    TestLinkAPI api = new TestLinkAPI(testlinkURL, DevKey);
    //TestLinkAPIClient api = new TestLinkAPIClient(DevKey,Server_URL); 
    try{
           if (status.trim().equalsIgnoreCase(TC.PASS)){
                 api.reportTCResult(testCaseID, null, testPlanID, ExecutionStatus.PASSED, buildNumber, null, notes, null, null, null, platformName, null, null);
           }

           if (status.trim().equalsIgnoreCase(TC.FAIL)){
                 api.reportTCResult(testCaseID, null, testPlanID, ExecutionStatus.FAILED, buildNumber, null, notes, null, null, null, platformName, null, null);
           }

           if (status.trim().equalsIgnoreCase(TC.BLOCK)){
                 api.reportTCResult(testCaseID, null, testPlanID, ExecutionStatus.BLOCKED, buildNumber, null, notes, null, null, null, platformName, null, null);
           }
           }catch(Exception e){
                 System.out.println("Could not find the parameter"+e);}
    }
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
//Method added by Avanish for testlink integration
public static void addTestCaseToTestPlan(String testProjectId, String testPlanId, String testCaseId, String testVersion,
      String platformName) throws TestLinkAPIException, MalformedURLException 
{
	try {
		int projectID=(int)Double.parseDouble(testProjectId);
		int planID=(int)Double.parseDouble(testPlanId);
		int tcID=(int)Double.parseDouble(testCaseId.trim());
		int version=(int)Double.parseDouble(testVersion);
		int platformId=getPlatformIDByName(projectID,platformName,TestLinkDevKey,TestLinkServerURL);
		//int externalID=(int)Double.parseDouble(TC.TestLinkExternalID);
		URL testlinkURL;
		
			testlinkURL = new URL(TestLinkServerURL);
			
			
			TestLinkAPI api = new TestLinkAPI(testlinkURL, TestLinkDevKey);
			
			TestCase tc=api.getTestCase(tcID, null, null);
			tc.getVersion();
			
		//api.getTestCaseIDByName(testCaseName, testSuiteName, testProjectName, testCasePathName)
			api.addTestCaseToTestPlan(projectID, planID, tcID, tc.getVersion().intValue(), platformId,1,1);
		} catch (Exception e) {
			Logs.Ulog("Problem adding test case to test plan....");
			//e.printStackTrace();
		}
}

//HHHHHHHHHHHHHHHHHHHHHHHHH
public static Integer getPlatformIDByName(Integer projectId,String Platformname,String DevKey,String Server_URL)throws TestLinkAPIException, MalformedURLException{
	
	DevKey=TestLinkDevKey;	
	Server_URL=TestLinkServerURL;
	URL testlinkURL;
	try {
		testlinkURL = new URL(Server_URL);
	
			
  TestLinkAPI api = new TestLinkAPI(testlinkURL, DevKey);
  Platform[] platforms=api.getProjectPlatforms(projectId);
  for(Platform itr:platforms){
	  if(itr.getName().equals(Platformname.trim())){
		  return itr.getId().intValue();}
  }
  return null;
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
		return 0;
	}
}
//Added by Avanish on 13-1-17
public static void assignTestCaseToTester(String testCaseId,String testPlanID, String user, String buildID,String platformName) throws TestLinkAPIException, MalformedURLException{
	String buildName="";
	URL testlinkURL = new URL(TestLinkServerURL);		
	try{
	TestLinkAPI api = new TestLinkAPI(testlinkURL, TestLinkDevKey);
    int planID=(int)Double.parseDouble(testPlanID);
    String platformId="";
    int tcid=(int)Double.parseDouble(testCaseId.trim());

    	Build []build=api.getBuildsForTestPlan(planID);
        for(Build itr:build){
     	   if(itr.getId().toString().equals(buildID)){
     		  buildName= itr.getName();
     		  break;
     	   }
        }
        Platform[] platform=api.getTestPlanPlatforms(planID);
        for(Platform itr:platform){
      	   if(itr.getName().toString().equals(platformName)){
      		 platformId= itr.getId().toString();
      		  break;
      	   }
         }
        Map<String, Object> executionData = new HashMap<String, Object>();
        
        executionData.put("url", TestLinkServerURL.toString());
        executionData.put( "devKey",TestLinkDevKey.toString());
        executionData.put( "testcaseid",tcid+"");
        executionData.put ("testplanid",planID+"");
        executionData.put( "buildname",buildName);
        executionData.put( "user",user);
        
        executionData.put( "platformid",platformId);
       /* TestCase tc=api.getTestCase(testCaseId, null, null);
      
        api.addPlatformToTestPlan(prjID, planID, TestLinkPlatform);
    	api.assignTestCaseExecutionTask(planID, tc.getFullExternalId(), user, buildName);*/
        api.executeXmlRpcCall("tl.assignTestCaseExecutionTask", executionData);
           }catch(Exception e){
                 System.out.println("Could not find the parameter"+e);}
    
	}
}