package Utility;

import org.apache.commons.lang3.StringEscapeUtils;

/**
   * Simple Java program to escape XML or HTML special characters in String.
  * There are five XML Special characters which needs to be escaped :
  *     & - &amp;
    < - &lt;
    > - &gt;
    " - &quot;
     ' - &apos;
  * @author http://javarevisited.blogspot.com
  */
public class XMLUtils {
   
  
    public static void main(String args[]) {
     
        //handling xml special character & in Java String
        String xmlWithSpecial = "Java & HTML"; //xml String with& as special characters
        System.out.println("Original unescaped XML String: " + xmlWithSpecial);
        System.out.println("Escaped XML String in Java: "
                           +  StringEscapeUtils.escapeXml(xmlWithSpecial));
      
        //handling xml special character > in String on Java
        xmlWithSpecial = "Java> HTML"; //xml String with & as special characters
        System.out.println("Original unescaped XML String: " + xmlWithSpecial);
         System.out.println("Escaped XML String : " + StringEscapeUtils.escapeXml(xmlWithSpecial));
      
        




        //handling xml and html special character < in String
        xmlWithSpecial = "Java< HTML"; //xml String with & as special characters
        System.out.println("Original unescaped XML String: " + xmlWithSpecial);
         System.out.println("Escaped XML String: " + StringEscapeUtils.escapeXml(xmlWithSpecial));
      
        




        //handling html and xml special character " in Java
        xmlWithSpecial = "Java \" HTML"; //xml String with & as special characters
        System.out.println("Original unescaped XML String: " + xmlWithSpecial);
         System.out.println("Escaped XML String: " + StringEscapeUtils.escapeXml(xmlWithSpecial));
      
        //handling xml special character ' in String from Java
        xmlWithSpecial = "Java ' HTML"; //xml String with & as special characters
        System.out.println("Original unescaped XML String: " + xmlWithSpecial);
         System.out.println("Escaped XML String: " + StringEscapeUtils.escapeXml(xmlWithSpecial));
    
    } 
}