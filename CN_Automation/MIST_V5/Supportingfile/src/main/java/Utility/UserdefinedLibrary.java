package Utility;

import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.parentFolder;
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.ios.IOSDriver;



import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.thoughtworks.selenium.webdriven.commands.Click;


public class UserdefinedLibrary extends StandardLibrary{

	public static JavascriptExecutor js;
	public static String emailid = "";
	public UserdefinedLibrary() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
	public static void Echo()
    {
		
   
		System.out.println("In Userdefined Function"  + TC.InputData);
   	
    }

	
	
	public static void EnterTExt() throws IOException, InterruptedException
    {
		
   
		 TORObj().sendKeys("\\% eval program_start=strptime(start_date, \"%Y-%m-%d\") \\% eval program_end =strptime(stop_date, \"%Y-%m-%d\") \\% eval search_earliest=relative_time(now(),\"@d\" )\\% eval search_latest=if(\"now\"=\"now\", now(), relative_time(now(),\"now\"))\\% where (info_min_time <= program_end AND info_max_time >= program_start) OR (program_start >= info_min_time and program_end <= info_max_time) \\% eval ad_group_id=adgroup_id\\% search (experience_id=\"ad\" OR experience_id=\"coupon\" OR experience_id=\"mfd\")  campaign_id=4669234d-e05a-459e-ab80-9919abd47bc3  (ad_group_id=\"*\")  country_code=USA\\% stats dc(external_id));    }");
		 	 
		 
		 
		 
		 
    }
		 public static String InputStoredTextVar1() throws IOException, InterruptedException
    { 
			  WebElement element = TORObj();     /* element.click();*/
			 try  
	 {    		// driver.switchTo().activeElement().click();

System.out.println("Started Executing InputSotordedVar1");

				 Logs.Ulog(" Executing InputSotordedVar1 ");

          //driver.switchTo().activeElement().clear();   

	   driver.switchTo().activeElement().sendKeys(StoreTable.get("Var1").toString()); 
	   
	   System.out.println("EnTERED qUERY TEXT");
	   return UpdateResult.Done();
	   
   }
   catch(Throwable T)
   {
	   System.out.println("Input error " + T.getMessage());
	   return CatchStatementWebElement(T.getMessage());
	  
   }
   
    }
   public static String SetAttributeSotordedVar1() throws IOException, InterruptedException {

		Logs.Ulog(" Executing SetAttributeSotordedVar1 ");

		try {
			WebElement Obj = TORObj();
			Logs.Ulog("SetAttribute on obj " + Obj.getText());

			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0]." + TC.Param1 + "='" + StoreTable.get("Var1").toString() + "'", driver.switchTo().activeElement());

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

   }
     /* public static String RoboMouseClickOnObj() 
   
				Logs.Ulog("Start of RoboMouseClickOnObj");
   		try {

      			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());
      			C_RoboMouse.robotPoweredClick();
      
      			return UpdateResult.Done();
      
      		} catch (Throwable e) {
      			return CatchStatementWebElement(e.getMessage());
      		}
      
      	}
      
   }*/

   public static String AppendRandomPrefixString() throws IOException, InterruptedException {
		try{
			Logs.Ulog("AppendRandomPrefixString keyword is started");
			SecureRandom random = new SecureRandom();
			String randStr= new BigInteger(32, random).toString(16);
			StoreTable.put(TC.Param1, randStr+TC.InputData);
			emailid = randStr+TC.InputData;
			randStr="";
			Logs.Ulog("InputData successfully prefixed with random String");
			return UpdateResult.UpdateStatus();
		 } catch (Throwable e) {
       	 
			 StoreTable.put(TC.Param1,TC.InputData);
       	Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
           return CatchStatementWebElement(e.getMessage());
       }
		}
   
   
   
   private static String cint(String fscnum) {
	// TODO Auto-generated method stub
	return null;
}

private static void elseif(boolean b) {
	// TODO Auto-generated method stub
	
}

public static String OfferSum() throws IOException, InterruptedException {
		try{
			Logs.Ulog("AppendRandomPrefixString keyword is started");
			
			String a= TC.Param2;
			String b= TC.Param3;
			String c=	TC.Param4;
			
			String a1= a.replace("�", "");
			String a2= a1.replace(",", ".");
	
			String b1= b.replace("�", "");
			String b2= b1.replace(",", ".");
			
			String c1= c.replace("�", "");
			String c2= c1.replace(",", ".");
			
			float num_a= Float.parseFloat(a2);
			float num_b= Float.parseFloat(b2);
			float num_c= Float.parseFloat(c2);
			
			float result=num_a+num_b+num_c;
			double roundOff = (double) Math.round(result * 100) / 100;
			String str =Double.toString(roundOff);
			String sumvalue1 =str.replace(".0", "");
			String sumvalue =sumvalue1.replace(".", ",");
			

			StoreTable.put(TC.Param1,sumvalue);
			return UpdateResult.UpdateStatus();
		 } catch (Throwable e) {
     	 
				StoreTable.put(TC.Param1,TC.InputData);	
     	Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
         return CatchStatementWebElement(e.getMessage());
     }
		}
   // Used to truncate the Zero from Euro value : 6,70 --6,7
   //its take value as Inputdata  and truncate it  and stored in  param1
   public static String EuroTruncateZero() throws IOException, InterruptedException {
		try{
			Logs.Ulog("TruncateZeroEuro");
			
			String abc= TC.InputData;
			
			 if(abc.contains(","))
			 {
				 if(abc.endsWith("0"))
				 {
					 abc = abc.substring(0,abc.length()- 1);
				 }
			 }
			 System.out.println("Not perform EuroTruncateZero");
			 
			StoreTable.put(TC.Param1,abc);
			return UpdateResult.UpdateStatus();
		 } catch (Throwable e) {
      	 

				StoreTable.put(TC.Param4,TC.InputData);	
      	Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
          return CatchStatementWebElement(e.getMessage());
      }
		}

 
  
   
  /* public static String AccountNumberGenerator() throws IOException, InterruptedException {
		try{
			Logs.Ulog("AccountNumberGenerator keyword is started");
			Long monoprix_Acc_No=(long) 9281072701234;
			WebElement Obj = TORObj();
			do{
				
			}
			return UpdateResult.UpdateStatus();
		 } catch (Throwable e) {
      	 //Updated by Avanish on 25/11/2016
			 StoreTable.put(TC.Param1,TC.InputData);
      	Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
          return CatchStatementWebElement(e.getMessage());
      }
		}
  
   
   */

   
   
  

   
 //block method for testlink integration
 		public static String checkBlockedStatus() throws IOException, InterruptedException {
 			try{
 			if(TC.failStepCtr>0){
 				UpdateResult.GlobalStatus=TC.FAIL;
 				TC.TestLinkStatus=TC.BLOCK;
 				return TC.BLOCK;
 			}
 			else return TC.PASS;
 			}
 			catch(Throwable e){
 				return TC.BLOCK;
 			}
 			

 		}
 		
 		//switch tabs

 	   public static String SendKeyCTRL_Tab() throws IOException, InterruptedException { 

 			try {
 				Logs.Ulog("Executing SendKeyTab");
 				HighlightElement();
 				//DbClickJS();
 			TORObj().sendKeys(Keys.chord(Keys.CONTROL + "\t"));
 				// System.exit(0);
 			return TC.PASS;
 			} catch (Throwable t) {
 				return CatchStatementWebElement(t.getMessage());
 			}
 	
 	}
 		
// 		public static void ChangeTrailing4Digits() throws IOException, InterruptedException {
// 			String intFscNum = "";
//			try{
//				Logs.Ulog("ChangeTrailing4Digits keyword is started");
//				File file = new File(System.getProperty("user.dir"));
//				File parentFolder = file.getParentFile();
//				ExcelObj Global = new ExcelObj(parentFolder+ "/Projects/" + "GlobalSetup.xlsm");	
//				ExcelObj Resource = new ExcelObj(parentFolder+ "/Projects/CN/" + "Resource_CN.xlsm");
//				String browser = Global.getCellData("GlobalSetup", "ConfigValue", 3);
//				String Fscnum = TC.InputData;
//				//int Fscnum1 = Integer.parseInt(Fscnum);
//				if (browser.equalsIgnoreCase("CHROME")){
//					intFscNum = Fscnum+"1";
//				}
//				else if (browser.equalsIgnoreCase("MOZILLA")){
//						intFscNum = Fscnum+"2";
//					}
//				else if (browser.equalsIgnoreCase("IE")){
//						intFscNum = Fscnum+"3";
//					}
//				String currentDir=System.getProperty("user.dir");
//				StoreTable.put(TC.Param1, intFscNum);			 
//				} catch (Throwable e) {
//	        	 //Updated by test on 06/12/2016
//				 //StoreTable.put(TC.Param1,TC.InputData);
//	        	Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
//	        }
//			}
 		
 		
 		
 		public static String ChangeFSCDigits() throws IOException, InterruptedException {
 			String intFscNum = "";
			try{
				Logs.Ulog("ChangeFSCDigits keyword is started");
				File file = new File(System.getProperty("user.dir"));
				File parentFolder = file.getParentFile();

				ExcelObj Global = new ExcelObj(parentFolder+ "/Projects/" + "GlobalSetup.xlsm");	
				ExcelObj Resource = new ExcelObj(parentFolder+ "/Projects/CCJP/" + "Resource_CCJP.xlsm");
				String browser = Global.getCellData("GlobalSetup", "ConfigValue", 3);
				String Fscnum = TC.InputData;
				
				
			
				if (browser.equalsIgnoreCase("CHROME")){
					intFscNum = Fscnum+"7";
				}
				else if (browser.equalsIgnoreCase("MOZILLA")){
						intFscNum = Fscnum+"2";
					}
				else if (browser.equalsIgnoreCase("IE")){
						intFscNum = Fscnum+"3";
					}
				else if (browser.equalsIgnoreCase("ANDROIDBROWSER")){
					intFscNum = Fscnum+"4";
				}
				else if (browser.equalsIgnoreCase("IOSBROWSER")){
					intFscNum = Fscnum+"5";
				}

				else
					intFscNum = Fscnum;
				String currentDir=System.getProperty("user.dir");
				StoreTable.put(TC.Param1, intFscNum);
				return UpdateResult.UpdateStatus();
				} catch (Throwable e) {
	        	 //Updated by test on 06/12/2016
				 //StoreTable.put(TC.Param1,TC.InputData);
	        	Logs.Ulog("Something went wrong with keyword: ChangeFSCDigits");
	        	return TC.PASS;
	        }
			}
 		
 		public static String ReturnCurrentDate() throws IOException, InterruptedException {
 			Date date = new Date();
 			Logs.Ulog("ReturnCurrentDate keyword is started");
 			try{
 				if(TC.InputData!=null && !TC.InputData.trim().equals("")){
 	 				SimpleDateFormat sdf = new SimpleDateFormat(TC.InputData.trim());
 	 				String formattedDate = sdf.format(date);
 	 				StoreTable.put(TC.Param1,formattedDate);
 	 				System.out.println(formattedDate);
 	 				
 	 				}else{
 	 					Logs.Ulog("ReturnCurrentDate keyword is started with no arguements for format in input data");
 	 					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
 	 					String formattedDate = sdf.format(date);
 	 					StoreTable.put(TC.Param1,formattedDate);
 	 					System.out.println(formattedDate);
 	 					
 	 				}
				Logs.Ulog("keyword: ReturnCurrentDate is Executed successfully!");
				return UpdateResult.UpdateStatus();
			 } catch (Exception e) {
	        	 //Updated by test on 06/12/2016
				System.out.println("Illegal arguement exception");
 				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
 				String formattedDate = sdf.format(date);
 				System.out.println(formattedDate);
				StoreTable.put(TC.Param1,formattedDate);
	        	Logs.Ulog("Something went wrong with keyword: ReturnCurrentDate");
	            return TC.PASS;
	        }
			}
 		
 		public static String getFullImagePathFromTestData() throws IOException, InterruptedException {
			try{
				Logs.Ulog("uploadImageFromTestadata keyword is started");
				String currentDir=System.getProperty("user.dir");
				String inputDataPath="";
			Path path=Paths.get(currentDir);
			inputDataPath=TC.TestDataPath+TC.InputData;
			
				
				
				StoreTable.put(TC.Param1, inputDataPath);
				
				return UpdateResult.UpdateStatus();
			 } catch (Throwable e) {
	        	 //Updated by test on 06/12/2016
				 StoreTable.put(TC.Param1,TC.InputData);
	        	Logs.Ulog("Something went wrong with keyword: getFullImagePathFromTestadata");
	            return CatchStatementWebElement(e.getMessage());
	        }
			}
 		
 		public static String inputTextFromParam1() throws IOException, InterruptedException {
			try{	         
				Logs.Ulog("inputTextFromParam1 keyword is started");
				TORObj().clear();
				TORObj().sendKeys(TC.TestDataPath+"receiptexample.jpeg");
				UpdateResult.ActualData = TC.Param1.trim();
				TC.ExpectedData = TC.Param1.trim();
				Logs.Ulog("InputText on obj Done ");
				return UpdateResult.UpdateStatus();
			 } catch (Throwable e) {
	        	 //Updated by test on 06/12/2016
				
	        	Logs.Ulog("Something went wrong with keyword: inputTextFromParam1");
	            return CatchStatementWebElement(e.getMessage());
	        }
			}
 		
 		public static String sendkeysctrs() throws IOException, InterruptedException {

            Logs.Ulog("Checking existance of element");
            try {   
                   
                  Robot r= new Robot();
                  r.keyPress(KeyEvent.VK_CONTROL);
                  r.keyPress(KeyEvent.VK_S);
                  r.keyRelease(KeyEvent.VK_S);
                  r.keyRelease(KeyEvent.VK_CONTROL);
                  
                  
//                rt.exec("d:\\print.exe")
            
              } catch (Throwable e) {
                  UpdateResult.ActualData = TC.FAIL;
                  TC.FailDescription = e.getMessage();
                  UpdateResult.UpdateStatus();
                  return TC.FAIL;
            }
             return ALM_UpdateStatus_HTMLAttchment;
     }
 		
 		
 		

 		
 		
 		public static String VerifyPdfTextContains() throws IOException, InterruptedException{
 	 		
 			try
 			{
 				
 	       PDDocument pddDocument=PDDocument.load( new File (TC.InputData) , "UTF-8");
 	       PDFTextStripper textStripper = new PDFTextStripper();
 	       String content = textStripper.getText(pddDocument);
 	       
 	       //String  nstr = new String (  dd);
 	       
 	      System.out.println(content);
 	      System.out.println(TC.ExpectedData);
          if(content.replaceAll("[^A-Za-z0-9]" , "").replaceAll(" ", "").contains((((TC.ExpectedData.replaceAll("[^A-Za-z0-9]" , "").replaceAll(" ", "")))))){
        	 
        	  UpdateResult.ActualData =content;
              TC.ExpectedData = content;
           }
          else
          {
        	  UpdateResult.ActualData =content;
              TC.ExpectedData = "Content is not macting ";
          }
          
    } catch (Throwable t) {
          return CatchStatementWebElement(t.getMessage());
    }
 			return UpdateResult.UpdateStatus();
 		
 		}	
 		
 		
 		
 		public static String VerifySizeOfList() throws IOException, InterruptedException {

            try {
            
                   Logs.Ulog("Executing CTRL A SortSizeofList");
                  List<WebElement> EleCollection = WebElementsTORObj();
                  int ItemList = EleCollection.size();

                  UpdateResult.ActualData = String.valueOf(ItemList);
                  TC.ExpectedData = String.valueOf(TC.ExpectedData);
                  if(UpdateResult.ActualData.equals(TC.ExpectedData)){
                         UpdateResult.UpdateStatus();
                  return TC.PASS; 
                   }
            } catch (Throwable t) {
                  return CatchStatementWebElement(t.getMessage());
            }
            return ALM_UpdateStatus_HTMLAttchment;
 }
 		
 		public static String VerifypopupExist() throws IOException, InterruptedException {

 			Logs.Ulog("Checking existance of element");
 			try {
 				if( TORObj().isDisplayed());
 				{
 					driver.findElementById("cookieDisc_close").click();

 				} 
 				return UpdateResult.Done();
 			} catch (Throwable e) {
 				UpdateResult.ActualData = TC.FAIL;
 				TC.FailDescription = e.getMessage();
 				UpdateResult.UpdateStatus();
 				return TC.FAIL;
 			}
 		
}

 		public static String CloseAlertPresent() throws InterruptedException, IOException {

 			try {

 				// Check the presence of alert
 				Alert alert = driver.switchTo().alert();
 				// Alert present; set the flag
 				//UpdateResult.ActualData = alert.getText();

 				// if present consume the alert
 				alert.accept();
 				//BrowserSync();
 				//TC.ExpectedData = "Alert Should not popup";
 				//return UpdateResult.UpdateStatus();
 				return TC.PASS;

 			} catch (NoAlertPresentException ex) {
 				// Alert not present

 				Logs.Ulog("Alert not present" + ex.getMessage());
 				return CatchStatementWebElement(ex.getMessage());
 			}

 		}

 		public static String callAutoIt() throws InterruptedException, IOException {

 			try {
 				File file = new File(System.getProperty("user.dir"));
				String path =  file+"\\payloads\\Fileupload1.exe";
 				Runtime.getRuntime().exec(path);

 			} catch (NoAlertPresentException ex) {
 				System.out.println("Exception occured");
 			}
 			
 			return UpdateResult.Done();

 		}
 		public static String deleteuser_api() throws IOException, InterruptedException{
 			try{
 			if (emailid != null) {
 				Logs.Ulog("Delete user:" + emailid);
 				File file = new File(System.getProperty("user.dir"));
 				File parentFolder = file.getParentFile();
 				ExcelObj Resource = new ExcelObj(parentFolder+ "/Projects/CN/" + "Resource_CN.xlsm");		
 				String xmlfile = Resource.getCellData("TestData", "Datavalue", 31);
 				String strpwd = Resource.getCellData("TestData", "Datavalue", 6);
 				String currentDir=System.getProperty("user.dir");
 				//String xmlpath = currentDir + xmlfile;
 				String batchfile = currentDir + "\\RunAutomationScripts.bat";
 				String SnappURL = Resource.getCellData("TestData", "Datavalue", 2);
 				String partnerid = Resource.getCellData("TestData", "Datavalue", 32); 				
 				String cmd = batchfile + " " + xmlfile + " " + emailid + " " + strpwd + " "+ SnappURL + " " + partnerid;
 				Runtime.getRuntime().exec ( "cmd.exe /C start" + cmd );
 				//int exitVal = process.waitFor();
 				Logs.Ulog("Deleted user:" + emailid);
// 				if (exitVal == 0) {
// 					Logs.Ulog("Deleted user:" + emailid);
// 				}
 				
 			}
 			else
 			{
 			Logs.Ulog("No user registered to delete");
 			}
 			}
 			catch (IOException e) {
 				e.printStackTrace();
 			}
 			
 			emailid = "";
 			return TC.PASS;

 		}
 		
 		public static String deleteuser_ui() throws IOException, InterruptedException{
 			try{
 			if (emailid != null) {
 				Logs.Ulog("Delete user:" + emailid);
 				File file = new File(System.getProperty("user.dir"));
 				File parentFolder = file.getParentFile();
 				ExcelObj Resource = new ExcelObj(parentFolder+ "/Projects/CCJP/" + "Resource_CCJP.xlsm");		
 				//String xmlfile = Resource.getCellData("TestData", "Datavalue", 31);
 				String strpwd = Resource.getCellData("TestData", "Datavalue", 6);
 				String SnappURL = Resource.getCellData("TestData", "Datavalue", 2);
 				//String partnerid = Resource.getCellData("TestData", "Datavalue", 32); 				
 				String currentDir=System.getProperty("user.dir");
 				//String xmlpath = currentDir + xmlfile;
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "\\src\\main\\java\\chromedriver.exe");
				DesiredCapabilities capability = DesiredCapabilities.chrome();						
				driver = new ChromeDriver(); 	
				driver.manage().window().maximize();
 				driver.navigate().to(SnappURL);
 				//TimeUnit.SECONDS.sleep(5);
 				driver.findElementById("login").sendKeys("srnagara");
 				driver.findElementById("password").sendKeys("Catalina@123");
 				driver.findElementByXPath("//input[@name='commit']").click();
 				//TimeUnit.SECONDS.sleep(5);
 				if (driver.findElementByXPath("//a[@href='/logout']").isDisplayed()){
 					driver.findElementByXPath("//a[text()='Members']").click();
 					//TimeUnit.SECONDS.sleep(5);
 					driver.findElementById("filter_email").sendKeys(emailid);
 					driver.findElementByXPath("//input[@value='Filter']").click();
 					//TimeUnit.SECONDS.sleep(2);
 					if (driver.findElementByXPath("//table[@class='onable odded center']//tr[2]/td[3]").isDisplayed()){
 						String stremail = driver.findElementByXPath("//table[@class='onable odded center']//tr[2]/td[3]").getText().trim();
 							if(emailid.equals(stremail)){
 								driver.findElementByXPath("//a[@href='#delete_member']").click();
 								TimeUnit.SECONDS.sleep(2);
 								CloseAlertPresent();
 								TimeUnit.SECONDS.sleep(3);
 								Logs.Ulog("Deleted user:" + emailid);
 								driver.findElementByXPath("//a[@href='/logout']").click();
 								TimeUnit.SECONDS.sleep(2);
 							}else {
 								Logs.Ulog("No user present to be selected");
 							}
 					}else {
							Logs.Ulog("No user present to be selected");
						}
 				}else {
						Logs.Ulog("Login to Snapp failed");
					}
// 				if (exitVal == 0) {
// 					Logs.Ulog("Deleted user:" + emailid);
// 				}
 				
 			}
 			else
 			{
 			Logs.Ulog("No user registered to delete");
 			}
 			}
 			catch (Throwable e) {
 				e.printStackTrace();
 			}
 			emailid = "";
 			FunctionLibrary.CloseBrowser();
 			return TC.PASS;

 		} 	
 		 public static String Upload_image() throws InterruptedException, IOException
 		  {
 		      try
 		      {
 		          //driver = new AndroidDriver<MobileElement>(endpoint,capabilities);
 		          AndroidDriver driver1 = (AndroidDriver) driver;
 		          String currentContext = driver1.getContext(); //Switching the contextView to NATIVE
 		          driver1.context("NATIVE_APP");
 		          
 		          
 		         //driver1.findElementByXPath("//*[@resource-id='receipts']").sendKeys("C:\\UploadFiles\\receiptexample.jpg");
 		        driver1.findElementByXPath("//*[@resource-id='receipts']").click();
 		          
 		          if (driver1.findElementByXPath("//*[@resource-id='com.android.packageinstaller:id/permission_allow_button']").isDisplayed())
 		          {
 		              driver1.findElementByXPath("//*[@resource-id='com.android.packageinstaller:id/permission_allow_button']").click();
 		              
 		              driver1.findElementByXPath("//*[@resource-id='com.android.packageinstaller:id/permission_allow_button']").click();
 		              
 		              
 		          }
 		          driver1.findElementByXPath("//*[@text='Camera']").click();
 		          
 		          driver1.findElementByXPath("//*[@resource-id='MENUID_SHUTTER' and @index='4']").click();
 		          
 		          driver1.findElementByXPath("//*[@resource-id='com.sec.android.app.camera:id/okay']").click();
 		          
 		          /*driver1.findElement(By.xpath("//*[@class='android.widget.ImageButton' and @index='0']")).click();
 		          
 		          driver1.findElement(By.xpath("//*[@class='android.widget.LinearLayout' and @index='2']")).click();
 		          
 		          driver1.findElement(By.xpath("//*[@resource-id='com.android.documentsui:id/icon_mime']")).click();
 		          
 		          driver1.findElement(By.xpath("//*[@class='android.widget.ImageButton' and @index='1']")).click();
 		          
 		          driver1.findElement(By.xpath("//*[@resource-id='android:id/title' and @text='List view']")).click();
 		          
 		          driver1.findElement(By.xpath("//*[@resource-id='android:id/title' and @index='0']")).click();*/
 		          driver1.context(currentContext); //Set the ContextView to Web from NATIVE
 		          //driver1.close();
 		          //driver1.quit();v
 		          return TC.PASS;
 		      }
 		      catch (Throwable e) {
 		          UpdateResult.ActualData = TC.FAIL;
 		          TC.FailDescription = e.getMessage();
 		          UpdateResult.UpdateStatus();
 		          return TC.FAIL;
 		        }
 		      //return ALM_UpdateStatus_HTMLAttchment;
 		  }
  		public static String ChangePEXDigits() throws IOException, InterruptedException {
 			String intPexNum = "";
			try{
				Logs.Ulog("ChangePexDigits keyword is started");
				File file = new File(System.getProperty("user.dir"));
				File parentFolder = file.getParentFile();
				ExcelObj Global = new ExcelObj(parentFolder+ "/Projects/" + "GlobalSetup.xlsm");	
				ExcelObj Resource = new ExcelObj(parentFolder+ "/Projects/CCJP/" + "Resource_CCJP.xlsm");
				String browser = Global.getCellData("GlobalSetup", "ConfigValue", 3);
				String Pexnum1 = Resource.getCellData("TestData", "Datavalue", 19);
				String Pexnum2 = Resource.getCellData("TestData", "Datavalue", 20);
				String Pexnum3 = Resource.getCellData("TestData", "Datavalue", 21);
				String Pexnum4 = Resource.getCellData("TestData", "Datavalue", 22);
				String Pexnum5 = Resource.getCellData("TestData", "Datavalue", 23);
				//String Fscnum = TC.InputData;
				//int Fscnum1 = Integer.parseInt(Fscnum);
				if (browser.equalsIgnoreCase("CHROME")){
					intPexNum = Pexnum4;
				}
				else if (browser.equalsIgnoreCase("MOZILLA")){
						intPexNum = Pexnum2;
					}
				else if (browser.equalsIgnoreCase("ANDROIDBROWSER")){
					intPexNum = Pexnum3;
				}
				else if (browser.equalsIgnoreCase("IOSBROWSER")){
					intPexNum = Pexnum4;
				}
				else if (browser.equalsIgnoreCase("IE")){
					intPexNum = Pexnum1;
					}
				String currentDir=System.getProperty("user.dir");
				StoreTable.put(TC.Param1, intPexNum);
				return UpdateResult.UpdateStatus();
				} catch (Throwable e) {
	        	 //Updated by test on 06/12/2016
				 //StoreTable.put(TC.Param1,TC.InputData);
	        	Logs.Ulog("Something went wrong with keyword: ChangePexDigits");
	        	return TC.PASS;
	        }
			}
  		
  		public static String UPCNotrim() throws IOException, InterruptedException {

            Logs.Ulog("Checking existance of element");
            try {   
                   
                 String obtained = TC.InputData.trim();
                 System.out.println("obtained is - "+obtained);
                 String[] arr = obtained.split(" ");
                 System.out.println("arr is - "+arr[0]);
                 StoreTable.put(TC.ExpectedData, arr[0]);
                 return UpdateResult.Done();
            
              } catch (Throwable e) {
                  UpdateResult.ActualData = TC.FAIL;
                  TC.FailDescription = e.getMessage();
                  UpdateResult.UpdateStatus();
                  return TC.FAIL;
            }
            // return ALM_UpdateStatus_HTMLAttchment;
     }
  		
  		public static String convertToUpperCase() throws IOException, InterruptedException {
            try{
                  String obtained = TC.InputData;
                  StoreTable.put(TC.ExpectedData, obtained.toUpperCase());
                  Logs.Ulog("InputData successfully prefixed with random String");
                  return TC.PASS;
            } catch (Throwable e) {
          return CatchStatementWebElement(e.getMessage());
            }
            }
  		
  		 public static String convertToLowerCase() throws IOException, InterruptedException {
             try{
                   String obtained = TC.InputData;
                   StoreTable.put(TC.ExpectedData, obtained.toLowerCase());
                   Logs.Ulog("InputData successfully prefixed with random String");
                   return TC.PASS;
             } catch (Throwable e) {
          return CatchStatementWebElement(e.getMessage());
             }
             }
  		  	
public static String FormatDate() throws IOException, InterruptedException
{

	try{
		System.out.println(java.time.LocalDate.now());
		String stdate = java.time.LocalDate.now().toString();
		StoreTable.put(TC.ExpectedData, stdate);
		return TC.PASS;
	}
	catch(Throwable e){
		Logs.Ulog("Something went wrong with keyword: FormatDate");
		return TC.FAIL;
	}
	}
}
