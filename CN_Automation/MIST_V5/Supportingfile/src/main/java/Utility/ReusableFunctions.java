package Utility;

import java.awt.Color;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import static Utility.R_Start.Resultdata;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.Testdata;
import static Utility.R_Start.PrvKeyword;
import static Utility.R_Start.Status;
import static Utility.R_Start.StoreTable;
import static gtaf.controller.Controller.Pbar;

public class ReusableFunctions extends R_Start {

	public ReusableFunctions() throws InterruptedException, IOException, ClassNotFoundException {
		super(ExecutionModUleName);
		// TODO Auto-generated constructor stub
	}

	public static int ReUseCurRow;
	public static int RFRowNum;
	public static int ReuseStartRowNum;
	public static int ReuseEnd;
	public static String ExecutionModUleName = "TestScripts.";
	public static String ReuseKeyStart;
	public static String ReuseKeyStartEnd;
	public static String TDKey;
	public static String TDValue;
	public static String  CurrentReusablefunction;
	public static int ReuseFunctionRowcount;
	
	public static void ExeCurrentReusbleFunction(String ReuseFunction)
			throws InterruptedException, IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Logs.Ulog("*********************** Starting of ExeCurrentReusbleFunction **************************** ");
		 CurrentReusablefunction = ReuseFunction;
		 ReuseFunctionRowcount = (Integer) StoreTable.get("End" + ReuseFunction);
		String StoreModulename = TC.TestModuleName;
		// Swipe the sheet name to reusable sheet from the current module sheet
		TC.TestModuleName = TC.ReusableFunctionsSheet;
		Logs.Ulog(ReuseFunction + " StartRow --- "
				+ StoreTable.get(ReuseFunction));
		Logs.Ulog(ReuseFunction + " Rowcount --- "
				+ StoreTable.get(ReuseFunction + "Count"));
		
		int StROw = 1;

		for (ReUseCurRow = (Integer) StoreTable.get(ReuseFunction); ReUseCurRow <= (Integer) StoreTable
				.get("End" + ReuseFunction); ReUseCurRow++) {

			TC.LoadReuseTestData(ReUseCurRow);
//			if (!TC.Module.equals("") && TC.TCID.equals(""))
				if (!TC.Module.equals(""))
				UpdateResult.UpDateModule();
				

			Logs.Ulog("=== Starting of Reusable Function === " + TC.Keyword
					+ "=== Execute =  " + TC.ExecuteTest);
			// System.out.println(" *********** Executing Keywords  ************  "
			// + TC.Keyword);
			if (TC.PauseExecution.equals("YY"))
			{
				 while (TC.PauseExecution.equals("YY")) {
					 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
			            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
			           Pbar.p.setBackground(Color.YELLOW);
			            TimeUnit.SECONDS.sleep(3);
			           Pbar.p.setBackground(Color.WHITE);
			        }
				 
				 TC.PauseExecution = "NN";
				 Pbar.p.setBackground(new Color(189,183,107));
			}
			
			
			if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
				Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
						+ "=== Execute =  " + TC.ExecuteTest);
				Logs.Ulog("Executing " + TC.TestObjects + "." + TC.Keyword
						+ " " + TC.InputData + " @ Row  " + ReUseCurRow
						+ " of " + StoreTable.get(ReuseFunction + "Count"));
                 int DiRow =(Integer) StoreTable.get("End" + ReuseFunction) - (Integer)ReUseCurRow;
				Pbar.Testprogress(ReUseCurRow, PrvKeyword,  " @ Step " + StROw++ + " of "
						+ StoreTable.get(ReuseFunction + "Count") + " "
						+ TC.TestObjects + "." + TC.Keyword + " "
						+ TC.InputData );

				UpdateResult.UpDateKeyword();
				PrvKeyword = TC.Keyword;
				
//				/&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
				
				if (TC.Keyword.startsWith("IF_ObjCondition") ) {
					Logs.Ulog("=== is a IF IF_ObjCondition =======");
					ConStates.ReuseIF_ObjCondition();
				}
				else if  (TC.Keyword.startsWith("IF_VarCondition") ) {
					Logs.Ulog("=== is a IF IF_VarCondition =======");
					ConStates.ResueIF_VarCondition();
				}
				else if  (TC.Keyword.startsWith("ForLoop")) {
					Logs.Ulog("=== is a Reuse keyword =======");
					
					ConStates.ForLoop();
					
				}
				else if (TC.Keyword.startsWith("ReuseForLoop")) {
					Logs.Ulog("=== is a Reuse keyword =======");
					
					ConStates.ReuseForLoop();
					
				}
				else if  (TC.Keyword.startsWith("ReuseIF_ObjCondition") ) {
					Logs.Ulog("=== is a IF IF_VarCondition =======");
					ConStates.ReuseIF_ObjCondition();
				}
				else if  (TC.Keyword.startsWith("ReUse_")) {
					Logs.Ulog("=== is a Reuse keyword =======");
					ReusableFunctions
							.ExeCurrentReusbleFunction(TC.Keyword);
				}
				else {
				
				try {
						if (!TC.Keyword.startsWith("IF_") && !TC.Keyword.startsWith("ReuseIF_") && !TC.Keyword.equalsIgnoreCase("End_IF") && !TC.Keyword.equalsIgnoreCase("End_ForLoop") )
						{
								Logs.Ulog("=== Starting Keywords === "
										+ TC.Keyword + "=== Execute =  "
										+ TC.ExecuteTest);
								// Class c =
								// Class.forName("FunctionLibrary.UtilityFunction");
								Class c = Class.forName("Utility.R_Start");
//								String TPAck_Class = ExecutionModUleName
//										+ TC.TestModuleName;
//								Class c = Class.forName(TPAck_Class);
								String KW = TC.Keyword;
								try
								{
									TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
									if (TC.Keyword.equals(null))
									{
										 TC.Keyword =  KW;
									}
								}
								 catch(Throwable t)
								 {
									 TC.Keyword =  KW;
								 }
								 if(TC.Keyword.equalsIgnoreCase("wait"))
										TC.Keyword = "Wait";
								Method m = c.getMethod(TC.Keyword, null);
								// m.invoke(null, null);
								Status = (String) m.invoke(null, null);
//added by Avanish to update testlink block status
								if(Status.equals(TC.BLOCK)){
									ReUseCurRow=(Integer) StoreTable
											.get("End" + ReuseFunction)+1;
									
									m=c.getMethod("CloseBrowser", null);
									m.invoke(null, null);
									return;
									
								}
								TC.stepCtr++;
								if(Status.equalsIgnoreCase(TC.FAIL)|| Status.equalsIgnoreCase("false")){
									TC.failStepCtr++;
								}
//Added till here
								Logs.Ulog("=== End of Keywords === "
										+ TC.Keyword + "=== Execute =  "
										+ TC.ExecuteTest);

								TimeUnit.SECONDS.sleep(Steptimedelay);
								Logs.Ulog("=== Execution Delay === "
										+ Steptimedelay);
								long Et = (System.currentTimeMillis() - startTime) / 1000;
								Logs.Ulog("------- EXECTION TIME TAKEN--------  = "
										+ String.valueOf(Et));
								StepSync();
						}
			

				} catch (Exception e) {
					Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
							+ TC.Keyword + " === Result State  " + PrvKeyword);
					Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
					UpdateResult.ActualData = TC.FAIL;
					TC.FailDescription =" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === ";
					UpdateResult.UpdateStatus();
				}
				}
				if(!(Status==null))
				{
					if (Status.equals("FAIL") && TC.OnFailExit.equalsIgnoreCase(TC.StopTestYes) )
					{						
						Logs.Ulog(" === Test Failed on  === "+ TC.Keyword + " === Result State " + PrvKeyword);
						break ;
				    }
				}
			} // End of Test Execution if
		}// End of test for
		// Reset the module name back to its original from reuse sheet
		TC.TestModuleName = StoreModulename;

	}// End of Function

		
	
	
public static void LoadReuseData() throws UnknownHostException {
		
		StoreTable.put("LoginUser", System.getProperty("user.name"));
//		java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
//		StoreTable.put("LoginMachine", localMachine);
		InetAddress address = InetAddress.getLocalHost(); 
	    String hostIP = address.getHostAddress().toString();
	    String hostName = address.getHostName().toString();

	    StoreTable.put("LoginMachineIP", hostIP);
		StoreTable.put("LoginMachine", hostName);
		
		
		Logs.Ulog("Started Loading LoadReuseData");
		
		Logs.Ulog("Started Loading LoadReuseData");
		for (RFRowNum = 1; RFRowNum <= Resourcedata
				.getRowCount(TC.ReusableFunctionsSheet); RFRowNum++) {

			if (Resourcedata.getCellData(TC.ReusableFunctionsSheet, TC.ModuleCol,
					RFRowNum).startsWith("ReUse_")) {
				ReuseKeyStart = Resourcedata.getCellData(TC.ReusableFunctionsSheet,
						TC.ModuleCol, RFRowNum);

				StoreTable.put(ReuseKeyStart, RFRowNum);

			}

			if (Resourcedata.getCellData(TC.ReusableFunctionsSheet, TC.ModuleCol,
					RFRowNum).startsWith("EndReUse_")) {
				ReuseKeyStartEnd = Resourcedata.getCellData(
						TC.ReusableFunctionsSheet, TC.ModuleCol, RFRowNum);
				StoreTable.put(ReuseKeyStartEnd, RFRowNum);
				int ReuseCount = (Integer) StoreTable.get(ReuseKeyStartEnd)
						- (Integer) StoreTable.get(ReuseKeyStart);

				StoreTable.put(ReuseKeyStart + "Count", ReuseCount);
			}

		}

		Logs.Ulog("End of Loading LoadReuseData");
	}

	public static void LoadTestData() throws UnknownHostException {	
		
		StoreTable.put("LoginUser", System.getProperty("user.name"));
		
		InetAddress address = InetAddress.getLocalHost(); 
	    String hostIP = address.getHostAddress().toString();
	    String hostName = address.getHostName().toString();

	    StoreTable.put("LoginMachineIP", hostIP);
		StoreTable.put("LoginMachine", hostName);
		
		Logs.Ulog("Started Loading LoadTestData");
		for (RFRowNum = 2; RFRowNum <= Resourcedata.getRowCount(TC.TestDataSheet); RFRowNum++) {
			for (int tclo = 0; tclo <= Resourcedata.getColumnCount(TC.TestDataSheet); tclo++) {

				//TDKey = Testdata.getCellData(TC.TestDataSheet,  tclo, RFRowNum);
				if (Resourcedata.getCellData(TC.TestDataSheet,  tclo, RFRowNum).startsWith("RD_")| Resourcedata.getCellData(TC.TestDataSheet,  tclo, RFRowNum).startsWith("D_"))
				{
					TDKey = Resourcedata.getCellData(TC.TestDataSheet,  tclo, RFRowNum);
								
					tclo++;
					try
				     {
						TDValue = ismodifydata(Resourcedata.getCellData(TC.TestDataSheet, tclo, RFRowNum));
						StoreTable.put(TDKey, TDValue);
				     }
				     catch(Exception e)
				     {
				    	 Logs.Ulog("Error while  LoadReuseData Resource data");
				     }
					StoreTable.put(TDKey, TDValue);
					
					
				}
				


			}

		}

		Logs.Ulog("End of Loading LoadTestData");
	}
	
}


