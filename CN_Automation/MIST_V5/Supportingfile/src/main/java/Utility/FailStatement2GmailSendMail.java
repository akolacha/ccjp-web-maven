package Utility;

 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import static Utility.R_Start.StoreTable;
import static Utility.HTMLResults.MsgBody;
public class FailStatement2GmailSendMail {

	
	public static File SendmailFile;
	public static BufferedWriter SendmailWrite;
	public static  String SenMailFilepath =  System.getProperty("java.io.tmpdir")+"/Fail2SendMail.vbs";
   

public static String WriteEmail() throws IOException
{
	try
	{
		
	
	   File SendmailFile = new File(SenMailFilepath); 		
	   SendmailFile.createNewFile();
	   
	    FileWriter FW = new FileWriter(SenMailFilepath);
	    SendmailWrite = new BufferedWriter(FW);
		
	        SendmailWrite.write(" Set iMsg = CreateObject(\"CDO.Message\")"+"\n");
	        SendmailWrite.write(" Set iConf = CreateObject(\"CDO.Configuration\")"+"\n");
     
			SendmailWrite.write("  iConf.Load -1  "+"\n");//' CDO Source Defaults
			SendmailWrite.write("   Set Flds = iConf.Fields "+"\n");
			SendmailWrite.write("  With Flds "+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/smtpusessl\") = True "+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/smtpauthenticate\") = 1 "+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/sendusername\") = \"" +StoreTable.get("FromEmailID")+"\""+"\n");
			SendmailWrite.write(".Item(\"http://schemas.microsoft.com/cdo/configuration/sendpassword\") = \""+StoreTable.get("FromEmailPassword")+"\""+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/smtpserver\") = \"" + StoreTable.get("Host")+"\""+"\n");
         
		    SendmailWrite.write(".Item(\"http://schemas.microsoft.com/cdo/configuration/sendusing\") = 2 "+"\n");
		    SendmailWrite.write(".Item(\"http://schemas.microsoft.com/cdo/configuration/smtpserverport\") =\""+ StoreTable.get("Port") +"\""+"\n");
		    SendmailWrite.write(".Update "+"\n");
			SendmailWrite.write("End With "+"\n");
			
			//SendmailWrite.write(" strbody = \"" + StoreTable.get("Body")+"\""+"\n");
		    
			SendmailWrite.write("  With iMsg "+"\n");
			SendmailWrite.write(" Set .Configuration = iConf "+"\n");
			SendmailWrite.write(" .To = \""+StoreTable.get("Fail2To")+"\""+"\n");
			SendmailWrite.write(".CC = \""+ StoreTable.get("Fail2CC")+"\""+"\n");
			SendmailWrite.write(" .BCC = \"" + StoreTable.get("Fail2BCC")+"\""+"\n");
			SendmailWrite.write(" .From = \"" +StoreTable.get("Fail2FromName")+"\""+"\n");
			SendmailWrite.write(" .Subject =\"" +StoreTable.get("Fail2Subject")+"\""+"\n");
			
			SendmailWrite.write(" .HTMLBody =\""+StoreTable.get("Fail2Body") + StoreTable.get("Fail2Signature")+"\""+"\n");
			SendmailWrite.write(" .AddAttachment (\""  + StoreTable.get("Fail2Attachment")+"\")"+"\n");
			
			SendmailWrite.write(" .Send"+"\n");     
			SendmailWrite.write("End With "+"\n");

			SendmailWrite.flush();
			SendmailWrite.close();
	return TC.PASS;
	}
	catch (Throwable e) {
		 Logs.Ulog("Error While Send Gmail Emai Fail2 Statement " + e.getMessage() );
		 return TC.PASS;
	}
	
}


public static String WriteEmailEnd() throws IOException
{
//	  Date date = new Date();
//	SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy-h_mm_ss a");
//    String formattedDate = sdf.format(date);
//    formattedDate =  formattedDate.replace("-", "").replace(" ", "");
//	   Zip.zip(ResultExcel.ResPathFolderDate.getAbsolutePath(), formattedDate);
//			FileWriter FW = new FileWriter(SenMailFilepath);
//		    SendmailWrite = new BufferedWriter(FW);
//     
//			//SendmailWrite.write(" strbody = " + StoreTable.get("Body"));
//    
//			SendmailWrite.write("  With iMsg ");
//			SendmailWrite.write(" Set.Configuration = iConf ");
//			SendmailWrite.write(" .To = "+StoreTable.get("To"));
//			SendmailWrite.write(".CC = " + StoreTable.get("CC"));
//			SendmailWrite.write(" .BCC = "+ StoreTable.get("BCC"));
//			SendmailWrite.write(" .From = "+StoreTable.get("FromName"));
//			SendmailWrite.write(" .Subject = "+StoreTable.get("Subject"));
//			SendmailWrite.write(" .HTMLBody =  "+ MsgBody );
//			SendmailWrite.flush();
//			//SendmailWrite.write(" .AddAttachment =  " + ResultExcel.ResultExlFilepath);
//			//SendmailWrite.write(" .Send");      
//			SendmailWrite.write("End With ");
//
//			SendmailWrite.flush();
	return TC.PASS;
	
}
}