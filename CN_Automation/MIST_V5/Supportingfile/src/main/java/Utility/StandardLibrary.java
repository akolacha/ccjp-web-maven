package Utility;

//Author Santhosha HC
//Date 28 April 2014

import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.S_Obj;
import static Utility.R_Start.Appiumserver;
import gtaf.controller.Server;
import io.appium.java_client.service.local.AppiumDriverLocalService;

import java.io.IOException;
import java.net.URL;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.XMLUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;






public class StandardLibrary extends TC {

	public StandardLibrary() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static long implicitWaitTime;
	public static ExcelObj Report;
	public static List<String> ItemList;
	public static Connection connection = null;
	public static String HighlightElement() throws InterruptedException,
			IOException {
		Logs.Ulog("Highlight on object");

		try {

			WebElement element = TORObj();
			for (int i = 0; i < 2; i++) {
				WrapsDriver wrappedElement = (WrapsDriver) element;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();

				js.executeScript(
						"arguments[0].setAttribute('style',arguments[1]);",
						element, "color: black ; border: 6px solid black;");
				
				js.executeScript(
						"arguments[0].setAttribute('style',arguments[1]);",
						element, "");
				
				// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
				// element, "color: black ; border: 6px solid black;");
				
				// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
				// element, "");
				
				
			}
			
			Logs.Ulog("Object Highlighted");
			return TC.PASS;
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While Highlight on object " + T.getMessage());
			CatchStatementWebElement("ERROR -- While Highlight on object "+ T.getMessage());
			return TC.FAIL;

		}

	}

// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String store() throws IOException, InterruptedException {
		try {
			Logs.Ulog("Store a Value ");
			StoreTable.put(TC.InputData, TC.TestObjects);
			Logs.Ulog("Store " + TC.TestObjects + " in " + TC.InputData);
			// System.out.println("Stored Variablename = " +
			// StoreTable.get(TC.TestObjects));
			TC.TestDescription = "Store " + TC.TestObjects + " in " + TC.InputData;			
			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While stor object " + T.getMessage());
			CatchStatementWebElement("ERROR -- While stor object "
					+ T.getMessage());
			return TC.FAIL;

		}

	}
	
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		public static String StoreSetup_Browser_device() throws IOException, InterruptedException {
			try {
				  Logs.Ulog("start UpdateSetupBrowser_device ");
				
				StoreTable.put("Previous_Browser_Device", StoreTable.get("Execute ON Browser"));
							StoreTable.get("Execute ON Browser");
					
				return UpdateResult.Done();
			} catch (Throwable T) {
				Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  " + T.getMessage());
				CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
						+ T.getMessage());
				return TC.FAIL;

			}

		}
		// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		public static String ResetSetup_Browser_device() throws IOException, InterruptedException {
			try {
				  Logs.Ulog("start UpdateSetupBrowser_device ");
				
				StoreTable.put("Execute ON Browser", StoreTable.get("Previous_Browser_Device"));
							
					
				return UpdateResult.Done();
			} catch (Throwable T) {
				Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  " + T.getMessage());
				CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
						+ T.getMessage());
				return TC.FAIL;

			}

		}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String Modify_Update_TestObject() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("Start of Modify_Update_TestObject");
			String TO = (String) StoreTable.get(TC.TestObjects);

			TC.FindByProp = TC.InputData;
			TC.FindByPropVal = ismodifydata(TC.ExpectedData);
			StoreTable.put(TC.TestObjects, TC.FindByProp + "~"
					+ TC.FindByPropVal);
			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While stor object " + T.getMessage());
			CatchStatementWebElement("ERROR -- While stor object "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String storeEval() throws IOException, InterruptedException {
		try {
			Logs.Ulog("Store Eval");

			TC.TestObjects = TS_Obj().getEval(TC.TestObjects);
			store();
			TC.TestDescription = "Store eval = " + TC.InputData;			
			
			return UpdateResult.Done();

		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While storEval object " + T.getMessage());

			CatchStatementWebElement("ERROR -- While storEval object "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	public static String verifyAlert() throws IOException, InterruptedException {
		try {
			Logs.Ulog("Executing  verifyAlert");

			UpdateResult.ActualData = TS_Obj().getAlert();
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();

		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While verifyAlert" + T.getMessage());
			CatchStatementWebElement("ERROR -- While storEval object "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String verifyAlertNotPresent() throws IOException, InterruptedException {
		try {
			Logs.Ulog(" Executing verifyAlertNotPresent");

			UpdateResult.ActualData = String.valueOf(TS_Obj().isAlertPresent());
			TC.ExpectedData = "false";
			return UpdateResult.UpdateStatus();

		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While verifyAlertNotPresent " + T.getMessage());
			return CatchStatementWebElement("ERROR -- While verifyAlertNotPresent object "
					+ T.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String verifyAlertPresent() {
		try {
			Logs.Ulog(" Executing verifyAlertPresent");

			UpdateResult.ActualData = String.valueOf(TS_Obj().isAlertPresent());
			TC.ExpectedData = "true";
			return UpdateResult.UpdateStatus();

		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While verifyAlertPresent" + T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String verifyAttributeContains() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing verifyAttributeContains  ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element  Attribute =="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0]." + TC.InputData, Obj));

			UpdateResult.ActualData = (String) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0]." + TC.InputData, Obj);

			if (UpdateResult.ActualData.contains(TC.Param1))
			{
				TC.ExpectedData = UpdateResult.ActualData ;
			}
			else
			{
				
				TC.ExpectedData = " Innertext not Contains --- " + TC.InputData;
			}
			       
			

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}
	
	

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String verifyAttribute() throws IOException,
	InterruptedException {

Logs.Ulog(" Executing verifyAttribute  ");

try {
	WebElement Obj = TORObj();

	// System.out.println(js.executeScript("return arguments[0].disabled=",
	// Obj));
	Logs.Ulog(" Verify element  Attribute =="
			+ ((JavascriptExecutor) driver).executeScript(
					"return arguments[0]." + TC.InputData, Obj));

	UpdateResult.ActualData = (String) ((JavascriptExecutor) driver)
			.executeScript("return arguments[0]." + TC.InputData, Obj);

	return UpdateResult.UpdateStatus();
}

catch (Throwable e) {
	return CatchStatementWebElement(e.getMessage());
}

}

// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String verifyBodyText() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing verifyBodyText  ");

		try {
			WebElement Obj = TORObj();

			UpdateResult.ActualData = TS_Obj().getBodyText();

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String storeAttribute() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing storeAttribute  ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element  Attribute =="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0]." + TC.InputData, Obj));

			StoreTable.put(TC.Param1, (String) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0]." + TC.InputData, Obj));
			TC.TestDescription = "storeAttribute " + TC.Param1 +" in " + TC.InputData  + " with value " +StoreTable.get(TC.Param1);			
			return UpdateResult.Done();
			
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String waitForElementPresent() throws IOException,
			InterruptedException

	{
		try {
			Logs.Ulog("wait for element present");
			ReSetSyncTime();
			int WT = (int) Double.parseDouble(TC.InputData);

			for (int i = 0; i <= WT; i++) {
				
				try {
					if (NonExceptionTORObj().isDisplayed()) {
						Logs.Ulog("Element Displayed ");
						SetDefaultSyncTime();
						return TC.PASS;
					}
				} catch (Throwable e) {

				}
			}
			Logs.Ulog("Unable to find the element / Element not present");
			SetDefaultSyncTime();

			return UpdateResult.Done();
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on waitForElementPresent = " + e.getMessage());
			return UpdateResult.UpdateStatus();

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	private static boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String storeXpathCount() throws IOException, InterruptedException {
		try {
			Logs.Ulog("Execute storeXpathCount ");

			StoreTable.put(TC.InputData, WebElementsTORObj().size());

			return TC.PASS;
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While storeXpathCount " + T.getMessage());
			return CatchStatementWebElement("ERROR -- While storeXpathCount " + T.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

		public static String VerifyXpathCount() throws IOException, InterruptedException {
			try {
				Logs.Ulog(" Executing VerifyXpathCount");

				UpdateResult.ActualData = String.valueOf(WebElementsTORObj().size());
				TC.ExpectedData = String.valueOf(TC.InputData);
				return UpdateResult.UpdateStatus();
			} catch (Throwable T) {
				Logs.Ulog("ERROR -- While VerifyXpathCount " + T.getMessage());
				return CatchStatementWebElement("ERROR -- While VerifyXpathCount " + T.getMessage());

			}

		}

		// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ReSetSyncTime() throws IOException, InterruptedException {
		try {
			
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

			return TC.PASS;
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While ReSetSyncTime " + T.getMessage());
			return CatchStatementWebElement("ERROR -- While ReSetSyncTime " + T.getMessage());

		}
	}

	public static String SetDefaultSyncTime() throws IOException, InterruptedException {
		try {
		
			implicitWaitTime = (int) Double.parseDouble((String) StoreTable
					.get("Browser time out"));
			driver.manage().timeouts()
					.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
			return TC.PASS;
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While ReSetSyncTime " + T.getMessage());
			return TC.FAIL;

		}
	}

	public static String SetSyncTime() throws IOException, InterruptedException {

		try {
			implicitWaitTime = (int) Double
					.parseDouble((String) TC.InputData);
			
			driver.manage().timeouts()
					.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);

			return TC.PASS;
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While SetSyncTime " + T.getMessage());
			
			return CatchStatementWebElement("ERROR -- While SetSyncTime " + T.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String waitForElementNotPresent() throws IOException,
			InterruptedException {
		Logs.Ulog("------ waitForElementNotPresent------");

		ReSetSyncTime();
		try {

			int WT = (int) Double.parseDouble(TC.InputData);
			for (int i = 0; i <= WT; i++) {

				try {

					if (TORObj().isDisplayed()) {

						Logs.Ulog("------Object Displayed----  ");
						
					}

				} catch (Throwable T) {
					SetDefaultSyncTime();
					break;
				}

			}

			Logs.Ulog("Executed---  waitForElementNotPresent   ");
			SetDefaultSyncTime();
			return UpdateResult.Done();
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on waitForElementNotPresent= "
					+ e.getMessage());
			return UpdateResult.UpdateStatus();

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static ExcelObj ExcelReport() {
		try {
			Report = new ExcelObj(TC.InputData);
			return Report;

		}

		catch (Throwable e) {

			Logs.Ulog("ERROR---   While Reading excel report / Or not have access to File = "
					+ e.getMessage());
			return Report;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String CatchStatementWebElement(String ErrMsg)
			throws IOException, InterruptedException {

		Logs.Ulog(TC.TestDescription);
		Logs.Ulog("ERROR ------ Unable to find the object " + ErrMsg);
		UpdateResult.ActualData = TC.FAIL;
		TC.FailDescription = "May be wrong inputs Script Error " + ErrMsg;
		UpdateResult.UpdateStatus();
		return TC.FAIL;
	}

	public static String BusyFormSync() throws IOException, InterruptedException {
		Logs.Ulog("------ BusyFormSync------");

		ReSetSyncTime();
		try {

			int WT = (int) Double.parseDouble(TC.InputData);
			for (int i = 0; i <= WT; i++) {

				try {

					if (TORObj().isDisplayed()) {

						Logs.Ulog("------Object Displayed----  ");
						
					}

				} catch (Throwable T) {
					SetDefaultSyncTime();
					break;
				}

			}

			Logs.Ulog("Executed---  BusyFormSync   ");
			SetDefaultSyncTime();
			return TC.PASS;
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on BusyFormSync = " + e.getMessage());
			return TC.PASS;

		}
	}
	
	public static String VerifyActiveElement() throws IOException, InterruptedException { 

		Logs.Ulog(" Executing VerifyActiveElement ");

		try {
					 
					Logs.Ulog(" Verify Active Attribute of ele == "	+ driver.switchTo().activeElement().getAttribute(TC.InputData));
					Logs.Ulog(" Verify Tobj() Attribute of ele == "	+ TORObj().getAttribute(TC.InputData));
			
				UpdateResult.ActualData = driver.switchTo().activeElement().getAttribute(TC.InputData);
				
				TC.ExpectedData = TORObj().getAttribute(TC.InputData);
			

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}
	
	
	public static String storeSelectedItemInList() throws IOException,
	InterruptedException {

Logs.Ulog(" Executing storeSelectedItemInList  ");

try {
	WebElement Obj = TORObj();

	// System.out.println(js.executeScript("return arguments[0].disabled=",
	// Obj));
	Logs.Ulog("storeSelectedItemInList ==");
	
	
	Select droplist = new Select(Obj);				
 
	StoreTable.put(TC.InputData , droplist.getFirstSelectedOption().getText());
	Logs.Ulog("storeSelectedItemInList  store " + droplist.getFirstSelectedOption().getText() +" in variable " + TC.InputData );
	TC.TestDescription = "storeSelectedItemInList " + TC.InputData +" in " + TC.InputData  + " with value " +StoreTable.get(TC.InputData);			
	return UpdateResult.Done();
	
}

catch (Throwable e) {
	return CatchStatementWebElement(e.getMessage());
}

}
	
	public static String VerifyselectdIteminList() throws IOException, InterruptedException { 

		Logs.Ulog("Verify  Selected item from list");
		TC.InputData = TC.InputData.replace(".0", "");
		TC.InputData = TC.InputData.replaceAll(" ", "");
		try {

			WebElement Ele = TORObj();
			
				Select droplist = new Select(Ele);				
				UpdateResult.ActualData = droplist.getFirstSelectedOption()
						.getText().replaceAll(" ","");
				TC.ExpectedData = TC.InputData.replaceAll(" ","");
				return UpdateResult.UpdateStatus();
			
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list Selecting from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}
	
	public static String VerifySortList() throws IOException, InterruptedException {

		try {
			boolean  Sort = true;
			Logs.Ulog("Executing CTRL A SortList");
			List<WebElement> EleCollection = WebElementsTORObj();
			ItemList = new ArrayList();
			for (int i = 0; i < EleCollection.size(); i++) {
				
						ItemList.add(EleCollection.get(i).getAttribute(TC.InputData));		
				
			}		
			Collections.sort(ItemList);
			for (int i = 0; i < EleCollection.size(); i++)
			{
				   if(!ItemList.get(i).toString().equals(EleCollection.get(i).getAttribute(TC.InputData).toString()))
				   {
					   Sort = false;
					    UpdateResult.ActualData = ItemList.get(i).toString()  ;
						TC.ExpectedData =EleCollection.get(i).getAttribute(TC.InputData) ;
					    UpdateResult.UpdateStatus();
					   break;
				   }	
			}
		
			if( Sort == true)
			{
				    UpdateResult.ActualData = "Items Sorted in Alphabetical order " ;
					TC.ExpectedData =  "Items Sorted in Alphabetical order " ;
				    UpdateResult.UpdateStatus();
			}
			
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}
		return TC.PASS;
		

	}
	
	public static String IFObjExist() throws IOException, InterruptedException { 

		Logs.Ulog("Checking existance of element");
		try {
		
			
			boolean a = NonExceptionTORObj().isDisplayed();
			Logs.Ulog("Object Displayed" + a);
			return "true";

		} catch (Throwable e) {

			Logs.Ulog("Object not Displayed" + e.getMessage());
			return "false";
		}

	}
	
	public static String IFObjisEnabled() throws IOException, InterruptedException { 

		Logs.Ulog("Checking existance of element");
		try {
		
			
			boolean a = NonExceptionTORObj().isEnabled();

		
			Logs.Ulog("Object isEnabled" + a);
			return "true";

		} catch (Throwable e) {

			Logs.Ulog("Object not Displayed" + e.getMessage());
			return "false";
		}

	}
	
	
	
	
	public static String ifEnabled() throws IOException, InterruptedException { 

		Logs.Ulog(" Executing InputText ");

		try {
			WebElement Obj = NonExceptionTORObj();

			// System.out.println(js.executeScript("return arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element disabled=="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0].disabled", Obj));
			boolean disabled = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].disabled", Obj);

			if (!disabled) 
			{
				Logs.Ulog("Object not disabled" +disabled);
				return "true";
			} else {
				Logs.Ulog("Object  disabled" +disabled);
				return "false";
			}
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}
	
	
	
	public static String ifnotHidden() throws IOException, InterruptedException { 

		Logs.Ulog(" Executing ifnotHidden ");

		try {
			WebElement Obj = NonExceptionTORObj();

			// System.out.println(js.executeScript("return arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element disabled=="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0].style.display", Obj));
			String hidden =  (String) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].style.display", Obj);

			if (hidden.equals("block")) 
			{
				Logs.Ulog("Object  is  not hidden" +hidden);
				return "true";
			} else {
				Logs.Ulog("Object  is hidden " +hidden);
				return "false";
			}
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String verifyGmapLatitude() throws IOException,
	InterruptedException {

Logs.Ulog(" Executing verifyGmapLatitude  ");

try {
	
	 WebElement Obj = TORObj();
		
	 JavascriptExecutor jse = (JavascriptExecutor)driver;
	
	  String Country   =  (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].value", Obj);
      String JS =  "var geocoder = new google.maps.Geocoder(); geocoder.geocode({'address': " + Country + " }, function (results, status) {alert (google.maps.GeocoderStatus.OK); var lat = results[0].geometry.location.lat(); return lat; });";
      String Lat   = (String)  jse.executeScript(JS);
 
	

	UpdateResult.ActualData = Lat;

	return UpdateResult.UpdateStatus();
}

catch (Throwable e) {
	return CatchStatementWebElement(e.getMessage());
}

}

// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

public static String verifyGmapLongitude() throws IOException,
InterruptedException {

		Logs.Ulog(" Executing verifyGmapLongitude  ");
		
	try {
		
		WebElement Obj = TORObj();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		
		String Country   =  (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].value", Obj);
		System.out.println(Country);
		String JS =  "var geocoder = new google.maps.Geocoder(); geocoder.geocode({'address': " + Country + " }, function (results, status) {alert (google.maps.GeocoderStatus.OK); var lng = results[0].geometry.location.lng();  return lng; });";
		String Lng   = (String)  jse.executeScript(JS);
		
		
		
		UpdateResult.ActualData = Lng;
		
		return UpdateResult.UpdateStatus();
		}
		
		catch (Throwable e) {
		return CatchStatementWebElement(e.getMessage());
		}

}
// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

public static String verifyGmapLoad() throws IOException,
InterruptedException {

		Logs.Ulog(" Executing verifyGmapLoad  ");
		
		try {
		
			WebElement Obj = TORObj();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			String Country   =  (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].value", Obj);
			System.out.println(Country);
//			 URL jqueryUrl = new URL("http://maps.googleapis.com/maps/api/js");
//             String jqueryText = Resources.toString(jqueryUrl, Charsets.UTF_8);
//             jse.executeScript(jqueryText);
			/*//String JS =  "var geocoder = new google.maps.Geocoder(); geocoder.geocode({'address': \""+ Country +"\" }, function (results, status) { if (status == google.maps.GeocoderStatus.OK) { return \"OK\" ; }else { return \"Not OK\" ;}});";
			String JS =  "var geocoder = new google.maps.Geocoder(); geocoder.geocode({'address': \"" + Country + "\" }, function (results, status) { var lng = results[0].geometry.location.lng();  } );{lng}";
			String Lat1   = TS_Obj().getEval(JS);
			System.out.println(Lat1);
            String Lat   = (String)  jse.executeScript(JS);
            System.out.println(Lat);
//			String JS =  "var geocoder = new google.maps.Geocoder(); geocoder.geocode({'address': " + Country + " }, function (results, status) {  return google.maps.GeocoderStatus.OK; });";
//			String Lng   = (String)  jse.executeScript(JS);
             JS =  "var geocoder = new google.maps.Geocoder(); geocoder.geocode({'address': \"" + Country + "\" }, function (results, status) { var lng = results[0].geometry.location.lng();  return lng; } return google.maps.GeocoderStatus.OK;);";
			
			String GmapLoad   =  (String) ((JavascriptExecutor) driver).executeScript(JS);
			System.out.println(GmapLoad);
			*/
			
			 String  Lat  = (String) jse.executeScript(" var geocoder = new google.maps.Geocoder();  return   geocoder.geocode( { 'address': " + "UAE" +"}, function(results, status) { 	var southWest = results[0].geometry.location.lat() ; var northEast = results[0].geometry.location.lng() ; } return northEast)");
				System.out.println(Lat );
		  Lat  = (String) jse.executeScript(" var geocoder = new google.maps.Geocoder();     geocoder.geocode( { 'address': " + "Dubai" +"}, function(results, status) { 	var southWest = results[0].geometry.location.lat() ; var northEast = results[0].geometry.location.lng() ; } return northEast");
				System.out.println(Lat );
			 Lat  = (String) jse.executeScript(" var geocoder = new google.maps.Geocoder();     geocoder.geocode( { 'address': " + "Dubai" +"}, function(results, status) { 	var southWest = results[0].geometry.location.lat() ; var northEast = results[0].geometry.location.lng() ; } return northEast");
				
			
			
//			String JS = "var geocoder = new google.maps.Geocoder(); geocoder.geocode( { 'address': \"" + Country + "\" }, function(results, status) { 	var southWest = results[0].geometry.location.lat() ; return results[0].geometry.location.lng() ; })  ";
//			String Res = TS_Obj().getEval("var geocoder = new google.maps.Geocoder(); var southWest; geocoder.geocode( { 'address': \"" + Country + "\" }, function(results, status) { 	southWest = results[0].geometry.location.lat() ;});{ southWest }  ");
//            String Lat   = (String)  jse.executeScript(JS);

            System.out.println("Lat" + Lat );
			
			//WebDriverBackedSelenium Selenium  = new  WebDriverBackedSelenium(driver, "http://afwcsdevw01.corp.al-futtaim.com");
			
			  Lat  = (String) jse.executeScript(" var geocoder = new google.maps.Geocoder();     geocoder.geocode( { 'address': " + "Dubai" +"}, function(results, status) { 	var southWest = results[0].geometry.location.lat() ; var northEast = results[0].geometry.location.lng() ; }) return northEast");
			  System.out.println("Lat" + Lat );
             Lat  = (String) jse.executeScript(" var geocoder = new google.maps.Geocoder();     geocoder.geocode( { 'address': " + "Dubai" +"}, function(results, status) { 	var southWest = results[0].geometry.location.lat() ; var northEast = results[0].geometry.location.lng() ; } )return northEast");
             System.out.println("Lat" + Lat );
			
			UpdateResult.ActualData = "OK";
			
			return UpdateResult.UpdateStatus();
		}
		
		catch (Throwable e)
		{
			return CatchStatementWebElement(e.getMessage());
		}

}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

public static String VerifyTableCellText() throws IOException,
InterruptedException {
	
		Logs.Ulog(" Executing VerifyTableCellText  ");
	
		try {
			WebElement Obj = TORObj();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			System.out.println( jse.executeScript("return arguments[0].rows(0).cells(0).innerText);" , Obj));
			System.out.println( jse.executeScript("return arguments[0].rows(1).cells(0).innerText);" , Obj));
			System.out.println( jse.executeScript("return arguments[0].rows(2).cells(0).innerText);" , Obj));
			        UpdateResult.ActualData = (String) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].rows('"+TC.InputData +"').cells('"+ TC.ExpectedData +"').innerText);" , Obj);
			        TC.ExpectedData =TC.Param1 ;
			return UpdateResult.UpdateStatus();
		}
	
		catch (Throwable e) {
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			return CatchStatementWebElement(e.getMessage());
		}

}



//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

public static String Db2GetRSSQL_Invemntory() throws SQLException, IOException, InterruptedException
{
	

	try
	{
		Logs.Ulog("Execute SQL GetRSSQL");
				
		Class.forName("com.ibm.db2.jcc.DB2Driver");
		//connection = DriverManager.getConnection("jdbc:db2://172.19.123.202:40001/QADB2", StoreTable.get("UserName") , StoreTable.get("Password"));
		connection = DriverManager.getConnection("jdbc:db2://172.19.123.202:40001/QADB2", (String) StoreTable.get("DBUserName") , (String) StoreTable.get("DBPassword"));
		Statement statement = connection.createStatement();
		 
		PreparedStatement preparedStatement = connection.prepareStatement(TC.InputData);
		 
		ResultSet resultSet = preparedStatement.executeQuery();
		String Out = "";
		while (resultSet.next()) {
		    String INVAVL_ID = resultSet.getString("INVAVL_ID");
		    String INVENTORYSTATUS = resultSet.getString("INVENTORYSTATUS");
		    String AVAILQUANTITY = resultSet.getString("AVAILQUANTITY");
		   
		    
		    Out =  Out + " INVAVL_ID = "+ INVAVL_ID+" |INVENTORYSTATUS = "+ INVENTORYSTATUS +" |AVAILQUANTITY= "+AVAILQUANTITY +"\n";
		    
			   
			  }
			System.out.println( Out );
			Logs.Ulog("Executed SQL Query Successfull - Db2GetRSSQL_Invemntory " +  Out );
			UpdateResult.ActualData = Out;
			UpdateDescription("Verify Inventory avalability in sap From Database SQL Statement = " + TC.InputData);
			return UpdateResult.Done();
	}		 
	catch(Throwable T)
	{
		Logs.Ulog("ERROR ----- Execute SQL Failed Check the Statement " + T.getMessage());
		return CatchStatementWebElement(T.getMessage());
	}
	
	
}


public static String Db2GetRSSQL_WCSDescFlags() throws SQLException, IOException, InterruptedException
{
	

	try
	{
		Logs.Ulog("Execute SQL GetRSSQL");
		
		Class.forName("com.ibm.db2.jcc.DB2Driver");
		//connection = DriverManager.getConnection("jdbc:db2://172.19.123.202:40001/QADB2", StoreTable.get("UserName") , StoreTable.get("Password"));
		connection = DriverManager.getConnection("jdbc:db2://172.19.123.202:40001/QADB2", (String) StoreTable.get("DBUserName") , (String) StoreTable.get("DBPassword"));
		Statement statement = connection.createStatement();
		 
		PreparedStatement preparedStatement = connection.prepareStatement(TC.InputData);
		 
		ResultSet resultSet = preparedStatement.executeQuery();
		String  Out = "" ;
		while (resultSet.next()) {
		    String IDENTIFIER = resultSet.getString("IDENTIFIER");
		    String VALUE = resultSet.getString("VALUE");
		    Out =  Out +  IDENTIFIER+" = "+ VALUE +"\n";
		    
		   
		  }
		System.out.println( Out );
		Logs.Ulog("Executed SQL Query Successfull - Db2GetRSSQL_WCSDescFlags " +  Out );
		UpdateResult.ActualData = Out;
		UpdateDescription("Verify WCS Management center Descripive attributes from Database SQL Statement = " + TC.InputData);
		
		return UpdateResult.Done();
		
		
		
			}
	 
	catch(Throwable T)
	{
		Logs.Ulog("ERROR ----- Execute SQL Failed Check the Statement " + T.getMessage());
		return CatchStatementWebElement(T.getMessage());
		
	}
	
	
}

public static String Db2GetRSSQL_Buffer() throws SQLException, IOException, InterruptedException
{
	

	try
	{
		Logs.Ulog("Execute SQL Db2GetRSSQL_Buffer");
				
		Class.forName("com.ibm.db2.jcc.DB2Driver");
		//connection = DriverManager.getConnection("jdbc:db2://172.19.123.202:40001/QADB2", StoreTable.get("UserName") , StoreTable.get("Password"));
		connection = DriverManager.getConnection("jdbc:db2://172.19.123.202:40001/QADB2", (String) StoreTable.get("DBUserName") , (String) StoreTable.get("DBPassword"));
		Statement statement = connection.createStatement();
		 
		PreparedStatement preparedStatement = connection.prepareStatement(TC.InputData);
		 
		ResultSet resultSet = preparedStatement.executeQuery();
		String Out = "";
		while (resultSet.next()) {
		    String FIELD1 = resultSet.getString("FIELD1");
		   
		    
		    Out =  Out + " FIELD1 = "+ FIELD1 + "\n";
		    
			   
			  }
			System.out.println( Out );
			Logs.Ulog("Executed SQL Query Successfull - Db2GetRSSQL_Buffer " +  Out );
			UpdateResult.ActualData = Out;
			UpdateDescription("Verify buffer  avalability in Management Center From Database SQL Statement = " + TC.InputData);
			return UpdateResult.Done();
	}		 
	catch(Throwable T)
	{
		Logs.Ulog("ERROR ----- Execute SQL Failed Check the Statement " + T.getMessage());
		return CatchStatementWebElement(T.getMessage());
		
	}
	
	
}



public static String FindElementBy_scrollDown() throws IOException, InterruptedException { 

	Logs.Ulog(" Start FindElementBy_scrollDown ");
	try {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		//js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
	//String  ST = (String) js.executeScript("return document.body.scrollHeight;");
		StoreTable.put("ScrollTop", js.executeScript("return document.body.scrollHeight;"));
		ReSetSyncTime();

		// int ST = Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
	    long Scrolltop =  (long) StoreTable.get("ScrollTop");
		for (long i = 0; i < (long)js.executeScript("return document.body.scrollHeight;") ; i=i+100) {
			// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));

			try {
				
				
				// ST =
				// Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
				
				try
				{
				if (NonExceptionTORObj().isDisplayed()) {
					

					HighlightElement();
					Logs.Ulog("Find element By SCrolling element has found ");
					SetDefaultSyncTime();
					return UpdateResult.Done();
					
					

				}
				}
				catch (Throwable t)
				{
					Logs.Ulog("Find element By SCrolling element has not found -  Scrolling page down ");
												
					js = (JavascriptExecutor) driver;
												
					js.executeScript("window.scrollTo(0,"+i+");");
					// ST = (int) js.executeScript("return document.body.scrollHeight;");

					
				}

			} catch (Throwable T) {
				for (int j = 0; j <= i; j++)
					TORObj().sendKeys(Keys.PAGE_UP);
			}

		}
		// System.out.println("**********************************************************************");
		UpdateResult.ActualData = "Element Not Found";
		TC.ExpectedData = "";
		Logs.Ulog("Click element By SCrolling element has found -  Fail");

		SetDefaultSyncTime();
		return UpdateResult.UpdateStatus();

	} catch (Throwable e) {
		SetDefaultSyncTime();
		//return CatchStatementWebElement(e.getMessage());
	}
	return TC.PASS;

}		
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
public static String FindElementBy_scrollUp() throws IOException, InterruptedException { 

	Logs.Ulog(" Start FindElementBy_scrollUp ");
	try {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
	//String  ST = (String) js.executeScript("return document.body.scrollHeight;");
		StoreTable.put("ScrollTop", js.executeScript("return document.body.scrollHeight;"));
		ReSetSyncTime();

		// int ST = Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
	    long Scrolltop =  (long) StoreTable.get("ScrollTop");
		for (long i = (long)js.executeScript("return document.body.scrollHeight;"); i > -100 ; i=i-100) {
			// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));

			try {
				
				
				// ST =
				// Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
				
				try
				{
				if ( NonExceptionTORObj().isDisplayed()) {
					

					HighlightElement();
					Logs.Ulog("Find element By SCrolling element has found ");
					SetDefaultSyncTime();
					return UpdateResult.Done();
					
					

				}
				}
				catch (Throwable t)
				{
					Logs.Ulog("Find element By SCrolling element has not found -  Scrolling page down ");
												
					js = (JavascriptExecutor) driver;
												
					js.executeScript("window.scrollTo(0,"+i+");");
					// ST = (int) js.executeScript("return document.body.scrollHeight;");

					
				}

			} catch (Throwable T) {
				for (int j = 0; j <= i; j++)
					TORObj().sendKeys(Keys.PAGE_UP);
			}

		}
		// System.out.println("**********************************************************************");
		UpdateResult.ActualData = "Element Not Found";
		TC.ExpectedData = "";
		Logs.Ulog("Click element By SCrolling element has found -  Fail");

		SetDefaultSyncTime();
		return UpdateResult.UpdateStatus();

	} catch (Throwable e) {
		SetDefaultSyncTime();
		//return CatchStatementWebElement(e.getMessage());
	}
	return TC.PASS;

}		
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

public static String PageScrollBottem() throws IOException, InterruptedException { 

				Logs.Ulog("PageScrollBottem");
				try {
					
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
					
					UpdateResult.Done();
				 

				} catch (Throwable e) {
					return CatchStatementWebElement(e.getMessage());
				}
				return TC.PASS;

			}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
public static String PageScrollTop() throws IOException, InterruptedException { 

Logs.Ulog("PageScrollTop");
try {
	
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollTo(0,0);");
	
	UpdateResult.Done();
 

} catch (Throwable e) {
	return CatchStatementWebElement(e.getMessage());
}
return TC.PASS;

}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

public static String StoreInnerTextMatchInExpectedData() throws IOException, InterruptedException { 

Logs.Ulog("StoreMatch");
try {   
WebElement Obj = TORObj();
WrapsDriver wrappedElement = (WrapsDriver) Obj;
JavascriptExecutor js = (JavascriptExecutor) driver;
wrappedElement.getWrappedDriver();
String mydata = (String) js.executeScript("return arguments[0].innerText", Obj);
Pattern pattern = Pattern.compile(TC.InputData);
Matcher matcher = pattern.matcher(mydata);

if (matcher.find())
{
StoreTable.put(TC.ExpectedData, matcher.group(1));
}
else
{
StoreTable.put(TC.ExpectedData, "Match not found");
}
UpdateResult.Done();
} 
catch (Throwable e) {
return CatchStatementWebElement(e.getMessage());
}
return TC.PASS;

}


//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
public static String ResizeBrowser() throws IOException, InterruptedException { 

Logs.Ulog(" ResizeBrowser Window ");
try {
	
int x = Integer.parseInt(TC.InputData );
int y = Integer.parseInt(TC.ExpectedData);
	
 Dimension d = new Dimension( x,y);
 driver.manage().window().setSize(d);
	UpdateResult.Done();
 

} catch (Throwable e) {
	return CatchStatementWebElement(e.getMessage());
}
return TC.PASS;

}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
public static String MoveBrowser() throws IOException, InterruptedException { 

Logs.Ulog(" ResizeBrowser Window ");
try {
	
int x = Integer.parseInt(TC.InputData );
int y = Integer.parseInt(TC.ExpectedData);
	
 Point  d = new Point( x,y);
 driver.manage().window().setPosition(d);
	UpdateResult.Done();
 

} catch (Throwable e) {
	return CatchStatementWebElement(e.getMessage());
}
return TC.PASS;

}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
public static String PassHighlightElement() throws InterruptedException {
Logs.Ulog("High light on object");
WebElement element = null;
try {
	element = NonExceptionTORObj();
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
try {
	
	for (int i = 0; i < 1; i++) {
		WrapsDriver wrappedElement = (WrapsDriver) element;
		JavascriptExecutor js = (JavascriptExecutor) driver;
		wrappedElement.getWrappedDriver();

		js.executeScript(
				"arguments[0].setAttribute('style',arguments[1]);",
				element, "color: black ; border: 4px solid darkgreen;");
		
		js.executeScript(
				"arguments[0].setAttribute('style',arguments[1]);",
				element, "");

	}
		
} catch (Throwable t) {
	Logs.Ulog("ERROR----Failed on High light on object");
}
// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
// element, "color: black ; border: 6px solid black;");

// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
// element, "");

Logs.Ulog("Object Highlighted");
return TC.PASS;
}	

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	public static String ErrorHighlightElement() throws InterruptedException, IOException {
		Logs.Ulog("ErrorHighlightElement High light on object");
		
		
				//ReSetSyncTime();
			
	
		try {
			
				WrapsDriver wrappedElement = (WrapsDriver) R_Start.FailTObj ;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();

				js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
						R_Start.FailTObj , "color: orange ; border: 6px solid orange;");

				//js.executeScript("arguments[0].setAttribute('style',arguments[1]);",element, "");

			
				return SetDefaultSyncTime();
				
		} catch (Throwable t) {
			Logs.Ulog("ERROR----Failed on Highlight on object");
			return SetDefaultSyncTime();
		}
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "color: black ; border: 6px solid black;");

		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "");
		
	}	
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ViewElement() throws IOException, InterruptedException

	{
		Logs.Ulog("Starting ViewElement");
		
		UpdateDescription( "Scroll to View the Object " + TC.TestObjects);
		
		try
		{
			WebElement Obj = NonExceptionTORObj();
			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Obj);
			
		}
		catch(Throwable T)
		{
			Logs.Ulog("Error while ViewElement" + T.getMessage());
		}
		
		return TC.PASS;

	}



	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

			public static void BrowserZoom50Per() throws IOException, InterruptedException { 

				Logs.Ulog("Starting of ZoomBrowser50Per ");

				try

				{			
							
							JavascriptExecutor js = (JavascriptExecutor) driver;
							js.executeScript("document.body.style.zoom='40%';");
							

				}

				catch (Throwable e)

				{
					Logs.Ulog("Error While ZoomBrowser50Per ");
					//return CatchStatementWebElement(e.getMessage());

				}

			}
			
			// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

					public static void BrowserZoom100Per() throws IOException, InterruptedException { 

						Logs.Ulog("Starting of ZoomBrowser50Per ");

						try

						{			
									
									JavascriptExecutor js = (JavascriptExecutor) driver;
									js.executeScript("document.body.style.zoom='100%';");
									

						}

						catch (Throwable e)

						{
							Logs.Ulog("Error While ZoomBrowser50Per ");
							//return CatchStatementWebElement(e.getMessage());

						}

					}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

					public static void UpdateDescription(  String TestDesc)
					{
						Logs.Ulog("Starting of UpdateDescription ");
						try
						{
							 if (TC.TestDescription.isEmpty())
							 {
								 TC.TestDescription = TestDesc;
							 }
						} 
						 catch (Throwable e)

							{

							 Logs.Ulog("Error in UpdateDescription "  + e.getMessage());

							}

					}
					
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
					
					public static String VerifyEmailGmailBySubject() 
					{
					   try {


					       String mailStoreType = "IMAP";
					        
					      

					   //create properties field
					   Properties properties = new Properties();

					   properties.put("mail.pop3.host", StoreTable.get("Host"));
					   properties.put("mail.pop3.port", "995");
					   properties.put("mail.pop3.starttls.enable", "true");
					   //Session emailSession = Session.getDefaultInstance(properties);
					   
					   Session emailSession = Session.getInstance(properties , new javax.mail.Authenticator() {
					 	    protected PasswordAuthentication getPasswordAuthentication() {
					 	        return new PasswordAuthentication( (String) StoreTable.get("ForgotemailGmailUser") , (String) StoreTable.get("ForgotemailGmailPassword"));
					 	    }
					 	});

					   //create the POP3 store object and connect with the pop server
					   Store store = emailSession.getStore("pop3s");

					   store.connect("smtp.gmail.com", (String) StoreTable.get("ForgotemailGmailUser") , (String) StoreTable.get("ForgotemailGmailPassword"));

					   //create the folder object and open it
					   Folder emailFolder = store.getFolder("INBOX");
					   emailFolder.open(Folder.READ_WRITE);

					   // retrieve the messages from the folder in an array and print it
					   Message[] messages = emailFolder.getMessages();
					   System.out.println("messages.length---" + messages.length);

					   for (int i = 0, n = messages.length; i < n; i++) {
					      Message message = messages[i];
					     
					    
					     if( message.getSubject().equalsIgnoreCase(TC.InputData))
					     {
					         TC.ExpectedData = message.getSubject();
					         StoreTable.put("GmailFrom" ,  message.getFrom()[0] );
					         StoreTable.put("GmailBody" ,  message.getContent().toString());
					         UpdateResult.ActualData = TC.InputData;
										
								    
								return UpdateResult.UpdateStatus();
					     }
					     else
					     {
					     UpdateResult.ActualData =TC.InputData;
									
							        TC.ExpectedData ="Email not Received" ;
							return UpdateResult.UpdateStatus();
						}
					     
					   }
					   

					   } catch (NoSuchProviderException e) {
					      e.printStackTrace();
					   } catch (MessagingException e) {
					      e.printStackTrace();
					   } catch (Throwable e) {
					      e.printStackTrace();
					   }
					return TC.PASS;
					}
					
					
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
					public static String DeleteAllInboxEmailGmail() 
					{
					   try {


					       String mailStoreType = "IMAP";
					        
					      

					   //create properties field
					   Properties properties = new Properties();

					   properties.put("mail.pop3.host", StoreTable.get("Host"));
					   properties.put("mail.pop3.port", "995");
					   properties.put("mail.pop3.starttls.enable", "true");
					   //Session emailSession = Session.getDefaultInstance(properties);
					   
					   Session emailSession = Session.getInstance(properties , new javax.mail.Authenticator() {
					 	    protected PasswordAuthentication getPasswordAuthentication() {
					 	        return new PasswordAuthentication( (String) StoreTable.get("ForgotemailGmailUser") , (String) StoreTable.get("ForgotemailGmailPassword"));
					 	    }
					 	});

					   //create the POP3 store object and connect with the pop server
					   Store store = emailSession.getStore("pop3s");

					   store.connect("smtp.gmail.com", (String) StoreTable.get("ForgotemailGmailUser") , (String) StoreTable.get("ForgotemailGmailPassword"));

					   //create the folder object and open it
					   Folder emailFolder = store.getFolder("INBOX");
					   emailFolder.open(Folder.READ_WRITE);

					   // retrieve the messages from the folder in an array and print it
					   Message[] messages = emailFolder.getMessages();
					   System.out.println("messages.length---" + messages.length);

					   for (int i = 0, n = messages.length; i < n; i++) {
					      Message message = messages[i];
					     					   
					      message.setFlag(Flags.Flag.DELETED, true);			
								    
								
					     }
					     emailFolder.close(true);
				         store.close();
					   return UpdateResult.UpdateStatus();
					   
					   } catch (NoSuchProviderException e) {
					      e.printStackTrace();
					   } catch (MessagingException e) {
					      e.printStackTrace();
					   } catch (Throwable e) {
					      e.printStackTrace();
					   }
					return TC.PASS;
					}
					
					
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
					// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHModified by Roopa for RSA  HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
					@SuppressWarnings("deprecation")
					public static String storeText() throws IOException, InterruptedException {
						try {
							Logs.Ulog("Store Text");

							
					
										
							StoreTable.put(TC.InputData ,  TORObj().getText().trim() );
							return UpdateResult.Done();

						} catch (Throwable T) {
							Logs.Ulog("ERROR -- While storEval object " + T.getMessage());

							CatchStatementWebElement("ERROR -- While storEval object "
									+ T.getMessage());
							return TC.FAIL;

						}

					}								
					public static String IF_VarCondition() throws IOException, InterruptedException { 

						Logs.Ulog(" Executing InputText ");
						

						try {
						       UpdateResult.ActualData =  TC.InputData;
						       return UpdateResult.UpdateStatus();
							// System.out.println(js.executeScript("return arguments[0].disabled=",
							
						}
						catch (Throwable e) {
							return CatchStatementWebElement(e.getMessage());
						}

					}
					
					public static String IF_VarEqual() throws IOException, InterruptedException { 

						Logs.Ulog(" Executing InputText ");
						

						try {
							
						       UpdateResult.ActualData =  Boolean.toString( true);
						       
						      int val1 = Integer.parseInt( TC.Param1);
						      int val2 = Integer.parseInt( TC.ExpectedData); 
						       
						       if(val1==val2 )
						       {
						    	   //UpdateResult.ActualData = TC.InputData;
						    	   return "true";
						       }else{
						    	   System.out.println("lsdflslsk88888888888888888888");
						    	   return "false";
						       }
						
							
						    	  
						       
						      // return UpdateResult.UpdateStatus();
							// System.out.println(js.executeScript("return arguments[0].disabled=",
							
						}
						catch (Throwable e) {
							return CatchStatementWebElement(e.getMessage());
						}

					}
                    //HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
					public static String IF_VarStrEqual() throws IOException, InterruptedException { 

						Logs.Ulog(" Executing InputText ");
						

						try {
							
						       UpdateResult.ActualData =  Boolean.toString( true);
						       
						     
						       
						       if(TC.Param1.equalsIgnoreCase(TC.ExpectedData ))
						       {
						    	   //UpdateResult.ActualData = TC.InputData;
						    	   return "true";
						       }else{
						    	   System.out.println("San if Condition str");
						    	   return "false";
						       }
						
							
						    	  
						       
						      // return UpdateResult.UpdateStatus();
							// System.out.println(js.executeScript("return arguments[0].disabled=",
							
						}
						catch (Throwable e) {
							return CatchStatementWebElement(e.getMessage());
						}

					}
                    //HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH  
                    public static String ValidateXMLNodes() throws Throwable {
                            
                           XMLUnit.setIgnoreWhitespace(true);
                            XMLUnit.setIgnoreAttributeOrder(true);
                            XMLUnit.setIgnoreComments(true);
                            XMLUnit.setIgnoreDiffBetweenTextAndCDATA(Boolean.TRUE);
                            
                            try {
                                Diff diff = new Diff(TC.InputData, TC.ExpectedData);
                               // System.out.println("Similar? " + diff.similar());
                                //System.out.println("Identical? " + diff.identical());
                                TC.TestDescription = "Validating Xml nodes ";
                             TC.ExpectedData = Boolean.toString( true );
                             UpdateResult.ActualData = Boolean.toString( diff.similar());
                             UpdateResult.UpdateStatus();
                                
                                
                     
                                DetailedDiff detDiff = new DetailedDiff(diff);
                                List<?> differences = detDiff.getAllDifferences();
                                for (Object object : differences) {
                                    Difference difference = (Difference)object;
                                   // System.out.println("***********************");
                                    //System.out.println(difference);
                                    
                                    String desc[] = difference.toString().split(" - comparing ");
                                     String AE[] =  desc[1].split(" to ");
                                     
                                    
                                    
                              TC.TestDescription = desc[0] ;
                              TC.ExpectedData  = "- Node details  " + AE[0];
                                    UpdateResult.ActualData = AE[1];
                                 UpdateResult.UpdateStatus();
                                   // System.out.println("***********************");
                                }
                             return UpdateResult.UpdateStatus();
                            } catch (Throwable e) {
                              return CatchStatementWebElement(e.getMessage());
                            }
                                       
                            
                         
             }

                    public static String WebServicePut() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePut");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePut();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}			
					
                    public static String WebServicePost() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePost");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePost();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}			
                    
                    
                    
                    public static String WebServicePostWithFormParameters() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePostWithFormParameters ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePostWithFormParameters();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServicePostWithFormParametersContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePostWithFormParameters ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePostWithFormParametersContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    public static String WebServicePostWithFormParametersGetStatusCode() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePostWithFormParametersGetStatusCode ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePostWithFormParametersGetStatusCode();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}

                    
                    public static String WebServiceGetWithFormParameters() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetWithFormParameters ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGetWithFormParameters();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServiceGetWithFormParametersContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetWithFormParametersContains ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGetWithFormParametersContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
	
                    public static String WebServiceGetWithFormParametersGetStatusCode() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetWithFormParametersGetStatusCode ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGetWithFormParametersGetStatusCode();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    public static String WebServiceGet() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGet");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGet();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}	
                    
                    
                    public static String WebServiceGetStoreKey() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetStoreKey");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGetStoreKey();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    
                    
                    public static String WebServicePutStoreKey() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetStoreKey");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePutStoreKey();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    public static String WebServicePutContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetStoreKey");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePutContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    
                    public static String WebServicePostStoreKey() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePutStoreKey");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePostStoreKey();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    public static String WebServicePatchStoreKey() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePatchStoreKey");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePatchStoreKey();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServiceDeleteStoreKey() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceDeleteStoreKey");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceDeleteStoreKey();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServiceGetContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGet");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGetContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    
                    
                    public static String WebServicePostContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGet");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePostContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServicePatchContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGet");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePatchContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServicePatch() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePatch");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePatch();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    public static String WebServiceDeleteVerifyStatusCode() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceDeleteVerifyStatusCode");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceDeleteVerifyStatusCode();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    public static String WebServiceDelete() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceDelete");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceDelete();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServiceDeleteContains() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceDelete");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceDeleteContains();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    
                    public static String WebServiceSoap() throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceSoap");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceSoap();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						

                	}
                    
                    public static String WebServiceGetVerifyStatusCode () throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServiceGetVerifyStatusCode ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServiceGetVerifyStatusCode();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						
                	}
                    
                    public static String WebServicePostVerifyStatusCode () throws IOException, InterruptedException 
                	{
                		Logs.Ulog("Verifying the WebServicePostVerifyStatusCode ");
                		try {
                			
                			ServiceLibrary Ws = new ServiceLibrary();
                			return Ws.WebServicePostVerifyStatusCode();

                		     }
                		catch (Throwable e)
                		{
                			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
                		 }
						
                	}
                 // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    public static void DisableScrollToView()
                   	 {    
                   	 try 
                   		 {
                   			 StoreTable.put("ScrolltoViewElement", TC.SettingsNo);
                   		 }
                   	 
                   	 catch(Throwable T)
                   		{
                   			Logs.Ulog("Error while DisableScrollToView  Please check your configurations in setup sheet " + T.getMessage());
                   		}
                    }
                   //HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    public static void EnableScrollToView()
                    {
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("ScrolltoViewElement", TC.SettingsYes);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while EnableScrollToView  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                    }

                   // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                   	
                    public static void EnableHighlightElement()
                    {
                   	 
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("HighlightElement", TC.SettingsYes);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while EnableHighlightElement  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                   	 
                    }
                    // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void DisableHighlightElement()
                    {
                   	
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("HighlightElement", TC.SettingsNo);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while DisableHighlightElement  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                   	 
                    }
                   	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    public static void EnableProxy()
                    {
                   	 
                   	 try 
                   	 {                  		 
                   			        System.getProperties().put("http.proxyHost", StoreTable.get("http_proxyHost"));
                   			        System.getProperties().put("http.proxyPort", StoreTable.get("http_proxyPort"));
                   			        System.getProperties().put("https.proxyHost", StoreTable.get("https_proxyHost"));
                   			        System.getProperties().put("https.proxyPort", StoreTable.get("https_proxyPort"));
                   			
                   	 }
                   catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while EnableProxy  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                   	                   	
                    }
                    
                    public static void EnableProxyGlobal()
                    {
                   	 
                    	if (((String) StoreTable.get("EnableProxy")).equalsIgnoreCase("YY"))
						{
	
		                   	 try 
		                   	 {                  		 
		                   			        System.getProperties().put("http.proxyHost", StoreTable.get("http_proxyHost"));
		                   			        System.getProperties().put("http.proxyPort", StoreTable.get("http_proxyPort"));
		                   			        System.getProperties().put("https.proxyHost", StoreTable.get("https_proxyHost"));
		                   			        System.getProperties().put("https.proxyPort", StoreTable.get("https_proxyPort"));
		                   			
		                   	 }
		                   catch(Throwable T)
		                   	{
		                   		Logs.Ulog("Error while EnableProxy  Please check your configurations in setup sheet " + T.getMessage());
		                   	}
                        }
                   	                   	
                    }
                    
                    
                   	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
 // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                   	
                    public static void EnableDesktopScreeShotOnError()
                    {
                   	 
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("DesktopScreeShotOnError", TC.SettingsYes);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while DesktopScreeShotOnError  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                    }
                   	 
 // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void DisableDesktopScreeShotOnError()
                    {
                   	
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("DesktopScreeShotOnError", TC.SettingsNo);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while DesktopScreeShotOnError  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                   	 
                    }  
                	
// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                   	
                    public static void EnableDesktopScreeShotOnEachStep()
                    {
                   	 
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("DesktopScreeShotOnEachStep", TC.SettingsYes);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while DesktopScreeShotOnEachStep  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                    }
                   	 
 // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void DisableDesktopScreeShotOnEachStep()
                    {
                   	
                   	 
                   	 try 
                   	 {
                   		 StoreTable.put("DesktopScreeShotOnEachStep", TC.SettingsNo);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while DesktopScreeShotOnEachStep  Please check your configurations in setup sheet " + T.getMessage());
                   	}
                   	 
                    }  
                    
 // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void StartSeneiumServer()
                    {
                   	
                   	 
                   	 try 
                   	 {
                   		Server startNewServer = new Server();
                   		startNewServer.startSeleniumServer();
                   		
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while StartSeneiumServer " + T.getMessage());
                   	}
                   	 
                    } 
// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void StopSeneiumServer()
                    {
                   	
                   	 
                   	 try 
                   	 {
                   		Server stopServer = new Server();
                   		stopServer.stopSeleniumServer(S_Obj);
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while StopSeneiumServer " + T.getMessage());
                   	}
                   	 
                    } 
// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void StartAppiumServer()
                    {
                   	
                   	 
                   	 try 
                   	 {
                   		                   		  
                   		TestAppiumServer AppiumServer= new TestAppiumServer();
                   		AppiumServer.startAppiumServer();
                   		
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while StartAppiumServer" + T.getMessage());
                   	}
                   	 
                    } 
// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void StopAppiumServer()
                    {
                   	
                   	 
                   	 try 
                   	 {    
                   		Appiumserver.stop();
                   	 }
                    
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while StopAppiumServer" + T.getMessage());
                   	}
                   	 
                    } 
// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    
                    public static void OneNotepadAndType()
                    {                 
                   	 	   DesktopOptions options= new DesktopOptions();
              			   options.setApplicationPath("C:\\WINDOWS\\system32\\notepad.exe");
              			  try{
              			   driver=new WiniumDriver(new URL("http://localhost:9999"),options);
              			   driver.findElementByClassName("Edit").sendKeys("Enter the text to notepad");
              			   
              			  	}                   
                    catch(Throwable T)
                   	{
                   		Logs.Ulog("Error while OneNotepadAndType" + T.getMessage());
                   	}
                   	 
                    }                
              
 // HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH                  
}
