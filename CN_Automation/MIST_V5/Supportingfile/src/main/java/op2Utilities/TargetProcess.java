package op2Utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
/*import com.relevantcodes.extentreports.ExtentReports;
 import com.relevantcodes.extentreports.ExtentTest;
 import com.relevantcodes.extentreports.LogStatus;*/


import entities.TestCase;
import entities.TestCaseRun;
import entities.TestPlan;
import entities.TestPlanRun;

public class TargetProcess {

	public static String TOKEN = "OTM3OndiakI0aTF3OTJESjg5SmdYb0t1eGYraFZVZWFGQnp0QVhEUTdoLy9pWVU9";

	public int testPlanRunID = 0;

	public Response createTestCase(TestCase testCase) {

		Client client = ClientBuilder.newClient();
		return client
				.target("https://catalina.tpondemand.com/api/v1/TestCases?access_token="
						+ TOKEN).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCase, MediaType.APPLICATION_JSON));
	}

	 public int createTestPlanRun(TestPlanRun testPlanRun) {
		 Gson gson=new Gson();
		 Gson g=new GsonBuilder().setPrettyPrinting().create();
		 System.out.println(g.toJson(testPlanRun));
	 Client client = ClientBuilder.newClient();
	 Response response =
	 client.target("https://catalina.tpondemand.com/api/v1/TestPlanRuns?access_token="
	 + TOKEN)
	 .request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
	 .post(Entity.entity(g.toJson(testPlanRun), MediaType.APPLICATION_JSON));
	 String data = response.readEntity(String.class);
	 JsonParser parser = new JsonParser();
	 JsonObject object = (JsonObject) parser.parse(data);
//	 Gson gson = new Gson();
	 System.out.println(gson.toJson(testPlanRun));
	 System.out.println(object);
	 System.out.println("Test Plan Run Created with id " +
	 object.get("Id").getAsInt());
	 return object.get("Id").getAsInt();
	 }

	public int getTestCaseRunId(int testPlanRunId, int testCaseId) {
		String url = "https://catalina.tpondemand.com/api/v1/TestCaseRuns/?access_token="
				+ TOKEN
				+ "&where=(TestCase.ID eq "
				+ testCaseId
				+ ") and(TestPlanRun.ID eq " + testPlanRunId + ")";
		Client client = ClientBuilder.newClient();
		Response response = client.target(url.replace(" ", "%20"))
				.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = ((JsonObject) parser.parse(data)).get("Items")
				.getAsJsonArray().get(0).getAsJsonObject();
		return object.get("Id").getAsInt();
	}

	public int updateStatus(int testPlanRunId, int testCaseId,
			TestCaseRun testCaseRun) {
		int testCaseRunId = getTestCaseRunId(testPlanRunId, testCaseId);
		Client client = ClientBuilder.newClient();
		Response response = client
				.target("https://catalina.tpondemand.com/api/v1/TestCaseRuns/"
						+ testCaseRunId + "?access_token=" + TOKEN)
				.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCaseRun, MediaType.APPLICATION_JSON));
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(data);
		return object.get("Id").getAsInt();
	}

	public List<Integer> bulkUpdate(int testPlanRunId,
			Map<Integer, TestCaseRun> map) {
		List<Integer> list = new ArrayList<>();

		for (Map.Entry entry : map.entrySet()) {
			int id = (int) entry.getKey();
			TestCaseRun testCaseRun = (TestCaseRun) entry.getValue();
			list.add(updateStatus(testPlanRunId, id, testCaseRun));

		}
		return list;
	}

	public int getBuild(int testPlanId, String buildName) {
		// TODO Auto-generated method stub

		// TargetProcess targetProcess = new TargetProcess();

		// int testPlanId =
		// Integer.parseInt(properties.getProperty("Test_Plan_ID"));
		// String buildName = properties.getProperty("Build_Name");
		TestPlanRun testPlanRun = new TestPlanRun();
		testPlanRun.setName(buildName);
		testPlanRun.setTestPlan(new TestPlan(testPlanId));
		 if(getBuildId(buildName, testPlanId)==-1)
		 testPlanRunID = createTestPlanRun(testPlanRun);

		return testPlanRunID;
	}

	public void bulkUpdate(int testbuild, List<Integer> testCaseIds,
			String status, String comment) {
		// TODO Auto-generated method stub

		// this.testPlanRunID = this.getBuildId(rF.getBuildName(),
		// Integer.parseInt(rF.getTestPlanID()));
		Map<Integer, Integer> testCaseIdsforMapping = null;

		try {
			testCaseIdsforMapping = new BufferedReader(new FileReader(new File(
					"testlinkAndTargetProcessId")))
					.lines()
					.map(p -> p.trim().split("\t"))
					.collect(
							Collectors.toMap(p -> Integer.parseInt(p[1]),
									p -> Integer.parseInt(p[0]),
									(old, news) -> {
										System.out.println(old + " " + news);
										return old;
									}));
		} catch (NumberFormatException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int testCaseId : testCaseIds) {

			TestCaseRun testCaseRun = new TestCaseRun(status, comment);
			try {
				System.out.println("Updating Test Case under testbuild "
						+ testPlanRunID + " testcase id "
						+ testCaseIdsforMapping.get(testCaseId));
				 updateStatus(testbuild,
				 testCaseIdsforMapping.get(testCaseId), testCaseRun);
				

			} catch (Exception exception) {
				System.out.println(exception);
				System.out.println("Not updated test case " + testCaseId);
			}
		}

	}

	public int getBuildId() {
		// TODO Auto-generated method stub
		return this.testPlanRunID;
	}

	public int getBuildId(String buildName, int testPlanId) {
		String url = "https://catalina.tpondemand.com/api/v1/TestPlans/"
				+ testPlanId + "/TestPlanRuns?access_token=" + TOKEN
				+ "&where=(name eq '" + buildName + "')";
		Client client = ClientBuilder.newClient();
		Response response = client.target(url.replace(" ", "%20"))
				.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonArray array = ((JsonObject) parser.parse(data)).get("Items")
				.getAsJsonArray();
		if (array.size() == 0)
			return -1;
		else
			this.testPlanRunID = array.getAsJsonArray().get(0)
					.getAsJsonObject().get("Id").getAsInt();
		return this.testPlanRunID;
	}

}
