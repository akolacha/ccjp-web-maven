/*********************************************************************************
/********************************************************************************
//Author : Raghavendra Pai & Santhosha H C
//Test   : Driver script V 3.0 
********************************************************************************
********************************************************************************/

package Driver;

import gtaf.controller.Controller;

import java.io.IOException;
import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import Utility.FunctionLibrary;
import Utility.Logs;
import Utility.TC;
import Utility.UserdefinedLibrary;
public class StartTest {

	public static void main(String[] args) throws Exception 
	{
		//TODO Auto-generated method stub
		
		Log();
		//KillProcess();
		System.out.println(System.getProperty("user.dir"));
		/*UserdefinedLibrary.SetBrowser_Globalexcel();
		UserdefinedLibrary.SetSuite_Resourceexcel();
		
		String NA[] = {args[0], args[1]};
		UserdefinedLibrary.SetEnvSuite_Globalexcel(NA);*/
		Controller Start = new Controller();
		Start.ExecuteModules();		
		FunctionLibrary.KillProcess();
		System.out.println("Execution Complete !!!!!!!!!!!!");		
		System.out.println("**** Thank You *****");		
		
	}
	
	
	public static void Log()
	{
		    Date now = new Date();
	        String Hr = String.valueOf( now.getHours());
	        System.setProperty("log4j.hours",Hr);
	        System.setProperty("log4j.minutes", String.valueOf(now.getMinutes()));
	        System.setProperty("log4j.seconds", String.valueOf(now.getSeconds()));
	        System.setProperty("log4j.month", String.valueOf(now.getMonth()));
	        System.setProperty("log4j.year", String.valueOf(now.getYear()));
	   
	}
//	public static String KillProcess() throws IOException, InterruptedException {
//
//		try {
//			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
//			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
//			Runtime.getRuntime().exec("TASKKILL /F /IM Safari.exe");
//			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
//			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
//			Logs.Ulog("Pocess Killed");
//			return TC.PASS;
//	// System.exit(0);
//		} catch (Throwable t) {
//			return TC.PASS;
//		}

//	}
		
	
}
