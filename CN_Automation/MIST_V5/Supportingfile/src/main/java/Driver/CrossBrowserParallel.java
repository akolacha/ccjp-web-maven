package Driver;
/*********************************************************************************
/********************************************************************************
//Author : Raghavendra Pai & Santhosha H C
//Test   : CrossBrowser execution script V 1.0 
********************************************************************************
********************************************************************************/

import static Utility.R_Start.parentFolder;
import Utility.ExcelObj;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import Utility.*;


public class CrossBrowserParallel  {
	public static  ExcelObj Resource;
	 public static void main (String args[]) throws InterruptedException, IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		 if(System.getProperty("excelFilePath")!=null){
				String filePath = System.getProperty("excelFilePath");
				
					Resource = new ExcelObj(filePath+ "/Projects/" + "GlobalSetup.xlsm");
				}
				else
				{
		 File file = new File(System.getProperty("user.dir"));
			parentFolder = file.getParentFile();
	        Resource = new ExcelObj(parentFolder+ "/Projects/" + "GlobalSetup.xlsm");
				}
	       String  Div_br =  Resource.getCellData("GlobalSetup", "ConfigValue" , 3);
			String[] list = Resource.getCellData("GlobalSetup", "ConfigValue" , 4).split("," );
			for( int Div =0 ; Div< list.length; Div++)
			{
				System.out.println("******** "+ list[Div] + " ********");
				Resource.setCellData("GlobalSetup", "ConfigValue", 3, list[Div]);
				
				String Testdir2 ="cmd /c start " +System.getProperty("user.dir")+ "\\Run.bat";
				Runtime.getRuntime().exec(Testdir2);
				
			}
			Resource.setCellData("GlobalSetup", "ConfigValue", 3, Div_br);
		//TODO Auto-generated method stub
		 
	}
		
		 
	        
	    
}
