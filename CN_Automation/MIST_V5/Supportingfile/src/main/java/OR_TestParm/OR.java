package OR_TestParm;

/*
 ********************************************************************************
 ********************************************************************************

 //  Start date 7 Jan 2014
 //  Author :Santhosha
 //  Test :  Object Repository script V 1.9 

 ********************************************************************************
 ********************************************************************************/
import static Utility.R_Start.SetupData;
import static Utility.R_Start.GlobalSetupData;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.TTlogs;
import static Utility.R_Start.Testrow;
import static Utility.R_Start.driver;
import static Utility.R_Start.Testdata;
import static Utility.R_Start.Resourcedata;
import static Utility.R_Start.parentFolder;
import static Utility.TC.ismodifydata;
import static Utility.TC.Reuseismodifydata;

import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
//import org.apache.log4j.TTCCLayout;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.google.common.base.Predicate;
import com.thoughtworks.selenium.SeleneseTestNgHelper;

import Utility.ExcelObj;
import Utility.FunctionLibrary;
import Utility.Logs;
import Utility.R_Start;
import Utility.StandardLibrary;
import Utility.TC;
import Utility.UpdateResult;
import static Utility.R_Start.TObj;
import static Utility.R_Start.StoreTable;
import static Utility.FunctionLibrary.Sync;
import static gtaf.controller.Controller.ModuleDriver;

public class OR {
	public static Hashtable SKeys;
	public static String Mkey = "";
	public static File myConfigtemp;
	public static String myResourcetemp;
	public static String myConfigfilepathTemp;
	public static String myResourcefilenameTemp;
	public static String Testdatatemp;
	public static String ProjectName;
	public static String mySetupPathTemp;

	public OR() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}

	// ObjectRepository
	public static WebElement TORObj() throws IOException, InterruptedException {

		try {

			switch (TC.FindByProp) {
			case "xpath": {
				if (StoreTable.get("ScrolltoViewElement").toString()
						.equalsIgnoreCase("YY"))
					ORViewElement(driver
							.findElement(By.xpath(TC.FindByPropVal)));

				if (StoreTable.get("HighlightElement").toString()
						.equalsIgnoreCase("YY"))
					FunctionLibrary.PassHighlightElement();

				R_Start.FailTObj = driver.findElement(By
						.xpath(TC.FindByPropVal));

				return driver.findElement(By.xpath(TC.FindByPropVal));
			}

			case "className":
				return driver.findElement(By.className(TC.FindByPropVal));

			case "cssSelector":
				return driver.findElement(By.cssSelector(TC.FindByPropVal));

			case "id": {
				ORViewElement(driver.findElement(By.id(TC.FindByPropVal)));

				if (StoreTable.get("HighlightElement").toString()
						.equalsIgnoreCase("YY"))
					FunctionLibrary.PassHighlightElement();

				R_Start.FailTObj = driver.findElement(By.id(TC.FindByPropVal));
				return driver.findElement(By.id(TC.FindByPropVal));
			}

			case "linkText":
				return driver.findElement(By.linkText(TC.FindByPropVal));

			case "name":
				return driver.findElement(By.name(TC.FindByPropVal));
			case "partialLinkText":
				return driver.findElement(By.partialLinkText(TC.FindByPropVal));
			case "tagName":
				return driver.findElement(By.tagName(TC.FindByPropVal));
			default:
				TTlogs.debug("InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp);
				UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription = "InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp;

				UpdateResult.UpdateStatus();

			}

		} catch (Throwable t) {

			TTlogs.debug("Test Object Unable to find the Test Object "
					+ TC.FindByPropVal + " By Using Property " + TC.FindByProp);
			Logs.Ulog(TC.TestDescription);
			Logs.Ulog("ERROR ------ Error While Executing SendKeys "
					+ t.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = "Application Objects Changed - Object Not Identified ---- "
					+ TC.TestObjects
					+ "----           "
					+ TC.FindByPropVal
					+ "          ---- "
					+ " By Using Property "
					+ TC.FindByProp
					+ "---- " + t.getMessage();

			UpdateResult.UpdateStatus();
		}
		return null;

	}

	public static WebElement NonExceptionTORObj() throws IOException,
			InterruptedException {

		try {

			switch (TC.FindByProp) {
			case "xpath":
				return driver.findElement(By.xpath(TC.FindByPropVal));

			case "className":
				return driver.findElement(By.className(TC.FindByPropVal));

			case "cssSelector":
				return driver.findElement(By.cssSelector(TC.FindByPropVal));

			case "id":
				return driver.findElement(By.id(TC.FindByPropVal));

			case "linkText":
				return driver.findElement(By.linkText(TC.FindByPropVal));

			case "name":
				return driver.findElement(By.name(TC.FindByPropVal));
			case "partialLinkText":
				return driver.findElement(By.partialLinkText(TC.FindByPropVal));
			case "tagName":
				return driver.findElement(By.tagName(TC.FindByPropVal));
			default:
				TTlogs.debug("InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp);
				UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription = "InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for --     "
						+ TC.FindByPropVal
						+ "     ---- By Using Property "
						+ TC.FindByProp;

				UpdateResult.UpdateStatus();
			}

		} catch (Throwable t) {
			TTlogs.debug("Test Object Unable to find the Test Object "
					+ TC.FindByPropVal + " By Using Property " + TC.FindByProp);
			Logs.Ulog("ERROR ------ Error While Executing SendKeys "
					+ t.getMessage());

		}
		return null;

	}

	public static List<WebElement> WebElementsTORObj() throws IOException,
			InterruptedException {

		try {

			if (TC.FindByProp.equals("xpath")) {

				return driver.findElements(By.xpath(TC.FindByPropVal));

			}

			if (TC.FindByProp.equals("className")) {

				return driver.findElements(By.className(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("cssSelector")) {

				return driver.findElements(By.cssSelector(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("id")) {

				return driver.findElements(By.id(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("linkText")) {

				return driver.findElements(By.linkText(TC.FindByPropVal));
			}
			if (TC.FindByProp.equals("name")) {

				return driver.findElements(By.name(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("partialLinkText")) {

				return driver
						.findElements(By.partialLinkText(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("tagName")) {

				return driver.findElements(By.tagName(TC.FindByPropVal));

			}

			if (TC.FindByProp.equals("")) {
				Logs.Ulog("InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp);
				TTlogs.debug("InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp);
				UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription = "InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp;

				UpdateResult.UpdateStatus();
			}

		} catch (Throwable t) {
			Logs.Ulog("Test Object Unable to find the Test Object By Find Elements "
					+ TC.FindByPropVal + " By Using Property " + TC.FindByProp);
			Logs.Ulog("ERROR ------ Error While Executing SendKeys "
					+ t.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = "Web Elements Application Objects Changed - Object Not Identified "
					+ "----     "
					+ TC.ObjectName
					+ "     ---- "
					+ TC.FindByPropVal
					+ "---- "
					+ " By Using Property "
					+ "----    " + TC.FindByProp + "    ---- " + t.getMessage();

			UpdateResult.UpdateStatus();
		}

		return null;

	}

	public static String SearchPTV(String WhichObject) {
		int ORRC = Testdata.getRowCount(TC.ObjectRepositorySheet);
		for (int ORRow = 1; ORRow <= ORRC; ORRow++) {

			if (Testdata.getCellData(TC.ObjectRepositorySheet,
					TC.ObjectNameCol, ORRow).equals(WhichObject)) {

				TC.FindByProp = Testdata.getCellData(TC.ObjectRepositorySheet,
						TC.FindByCol, ORRow);
				TC.FindByPropVal = Testdata.getCellData(
						TC.ObjectRepositorySheet, TC.PropertiesCol, ORRow);
				// Logs.Ulog("Get Property Type " + TC.FindByProp +
				// " Property Value  " + TC.FindByPropVal ) ;

				return TC.PASS;

			}

		}

		// Logs.Ulog("FInd by name =  " + TC.FindByProp + " Property Val  " +
		// TC.FindByPropVal + "  Object Searched  " + WhichObject ) ;

		if (TC.FindByProp.equals("") && TC.FindByPropVal.equals("")) {
			TC.FindByProp = Testdata.getCellData(TC.ObjectRepositorySheet,
					TC.FindByCol, Testrow);
			TC.FindByPropVal = Testdata.getCellData(TC.ObjectRepositorySheet,
					TC.PropertiesCol, Testrow);
			Logs.Ulog("Object not found in repository Proptype "
					+ TC.FindByProp + " Property Value  " + TC.FindByPropVal);

			return TC.FAIL;

		}
		return TC.PASS;
	}

	public static void LoadObjects() {
		StoreTable = new Hashtable();
		StoreTable.put("ScriptPause", "NN");
		int ORRC = Resourcedata.getRowCount(TC.ObjectRepositorySheet);
		for (int ORRow = 1; ORRow <= ORRC; ORRow++) {

			TC.ObjectName = Resourcedata.getCellData(TC.ObjectRepositorySheet,
					TC.ObjectNameCol, ORRow);
			TC.FindByProp = Resourcedata.getCellData(TC.ObjectRepositorySheet,
					TC.FindByCol, ORRow);
			TC.FindByPropVal = Resourcedata.getCellData(
					TC.ObjectRepositorySheet, TC.PropertiesCol, ORRow);
			// Logs.Ulog("Get Property Type " + TC.FindByProp +
			// " Property Value  " + TC.FindByPropVal ) ;

			StoreTable.put(TC.ObjectName, TC.FindByProp + "~"
					+ TC.FindByPropVal);

		}

	}

	public static String getORObject(String WhichObject) {
		try {
			String ORobj = (String) StoreTable.get(WhichObject);

			String OR[] = ORobj.split("~");
			TC.FindByProp = OR[0];
			TC.FindByPropVal = StringEscapeUtils
					.unescapeJava(ismodifydata(OR[1]));

			// String str = StringEscapeUtils.unescapeJava(TC.FindByPropVal);
			// System.out.println(str);

			return TC.PASS;
		} catch (Exception e) {
			Logs.Ulog("ERROR------Object not found in the repository Please correct the object name column");

			// return FunctionLibrary.CatchStatementWebElement(e.getMessage());

		}
		return TC.PASS;

	}

	public static String getWinORObject(String WhichObject) {
		try {
			String ORobj = (String) StoreTable.get(WhichObject);

			String OR[] = ORobj.split("~");
			TC.WinTitle = StringEscapeUtils.unescapeJava(ismodifydata(OR[0]));
			TC.ControlText = StringEscapeUtils
					.unescapeJava(ismodifydata(OR[1]));
			TC.ControlID = StringEscapeUtils.unescapeJava(ismodifydata(OR[2]));

			return TC.PASS;
		} catch (Exception e) {
			Logs.Ulog("ERROR------Object not found in the repository Please correct the object name column");

			// return FunctionLibrary.CatchStatementWebElement(e.getMessage());

		}
		return TC.PASS;

	}

	public static String ReusegetORObject(String WhichObject) {
		try {
			String ORobj = (String) StoreTable.get(WhichObject);

			String OR[] = ORobj.split("~");
			TC.FindByProp = OR[0];
			TC.FindByPropVal = Reuseismodifydata(OR[1]);

			return TC.PASS;
		} catch (Exception e) {
			Logs.Ulog("ERROR------Object not found in the repository Please correct the object name column");

			// return FunctionLibrary.CatchStatementWebElement(e.getMessage());

		}
		return TC.PASS;

	}

	public static String SendKeys() throws IOException, InterruptedException {
		Logs.Ulog("Executing Send keys");
		try {

			Mkey = "";
			if (SKeys == null)
				LoadSendkeys();

			WebElement Obj = TORObj();
			// Actions KeyBoard = new Actions(driver);
			// KeyBoard.click(Obj).perform();
			// KeyBoard.moveToElement(Obj).perform();
			// String key = (String) SKeys.get(TC.InputData);

			// ******************************************************************'
			String[] InKeys = TC.InputData.split("\\+");

			for (int K = 0; K < InKeys.length; K++) {
				String Keycoll = (String) SKeys.get(InKeys[K]);

				if (InKeys.length > 1 && K < (InKeys.length - 1)) {

					// Mkey = Mkey + Keycoll + ",";
					Mkey = Mkey + Keycoll; // + "+"
				} else {
					Mkey = Mkey + Keycoll;
				}

			}
			// ******************************************************************

			Obj.sendKeys(Mkey);

			Mkey = "";

			Logs.Ulog(" Executed SendKeys " + Obj.getText());

			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;
			return UpdateResult.UpdateStatus();
		} catch (Exception e) {
			Logs.Ulog(TC.TestDescription);
			Logs.Ulog("ERROR ------ Error While Executing SendKeys "
					+ e.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = "Error While Executing SendKeys -- "
					+ e.getMessage();

			return UpdateResult.UpdateStatus();
		}

	}

	public static String LoadSendkeys() {
		SKeys = new Hashtable();

		SKeys.put("NULL", "\uE000");
		SKeys.put("CANCEL", "\uE001");
		SKeys.put("HELP", "\uE002");
		SKeys.put("BACK_SPACE", "\uE003");
		SKeys.put("TAB", "\uE004");
		SKeys.put("CLEAR", "\uE005");
		SKeys.put("RETURN", "\uE006");
		SKeys.put("ENTER", "\uE007");
		SKeys.put("SHIFT", "\uE008");
		SKeys.put("LEFT_SHIFT", "\uE008");
		SKeys.put("CONTROL", "\uE009");
		SKeys.put("LEFT_CONTROL", "\uE009");
		SKeys.put("ALT", "\uE00A");
		SKeys.put("LEFT_ALT", "\uE00A");
		SKeys.put("PAUSE", "\uE00B");
		SKeys.put("ESCAPE", "\uE00C");
		SKeys.put("SPACE", "\uE00D");
		SKeys.put("PAGE_UP", "\uE00E");
		SKeys.put("PAGE_DOWN", "\uE00F");
		SKeys.put("END", "\uE010");
		SKeys.put("HOME", "\uE011");
		SKeys.put("LEFT", "\uE012");
		SKeys.put("ARROW_LEFT", "\uE012");
		SKeys.put("UP", "\uE013");
		SKeys.put("ARROW_UP", "\uE013");
		SKeys.put("RIGHT", "\uE014");
		SKeys.put("ARROW_RIGHT", "\uE014");
		SKeys.put("DOWN", "\uE015");
		SKeys.put("ARROW_DOWN", "\uE015");
		SKeys.put("INSERT", "\uE016");
		SKeys.put("DELETE", "\uE017");
		SKeys.put("SEMICOLON", "\uE018");
		SKeys.put("EQUALS", "\uE019");
		SKeys.put("NUMPAD0", "\uE01A");
		SKeys.put("NUMPAD1", "\uE01B");
		SKeys.put("NUMPAD2", "\uE01C");
		SKeys.put("NUMPAD3", "\uE01D");
		SKeys.put("NUMPAD4", "\uE01E");
		SKeys.put("NUMPAD5", "\uE01F");
		SKeys.put("NUMPAD6", "\uE020");
		SKeys.put("NUMPAD7", "\uE021");
		SKeys.put("NUMPAD8", "\uE022");
		SKeys.put("NUMPAD9", "\uE023");
		SKeys.put("MULTIPLY", "\uE024");
		SKeys.put("ADD", "\uE025");
		SKeys.put("SEPARATOR", "\uE026");
		SKeys.put("SUBTRACT", "\uE027");
		SKeys.put("DECIMAL", "\uE028");
		SKeys.put("DIVIDE", "\uE029");
		SKeys.put("F1", "\uE031");
		SKeys.put("F2", "\uE032");
		SKeys.put("F3", "\uE033");
		SKeys.put("F4", "\uE034");
		SKeys.put("F5", "\uE035");
		SKeys.put("F6", "\uE036");
		SKeys.put("F7", "\uE037");
		SKeys.put("F8", "\uE038");
		SKeys.put("F9", "\uE039");
		SKeys.put("F10", "\uE03A");
		SKeys.put("F11", "\uE03B");
		SKeys.put("F12", "\uE03C");
		SKeys.put("A", "A");
		SKeys.put("B", "B");
		SKeys.put("C", "C");
		SKeys.put("D", "D");
		SKeys.put("E", "E");
		SKeys.put("F", "F");
		SKeys.put("G", "G");
		SKeys.put("H", "H");
		SKeys.put("I", "I");
		SKeys.put("J", "J");
		SKeys.put("K", "K");
		SKeys.put("L", "L");
		SKeys.put("M", "M");
		SKeys.put("N", "N");
		SKeys.put("O", "O");
		SKeys.put("P", "P");
		SKeys.put("Q", "Q");
		SKeys.put("R", "R");
		SKeys.put("Q", "Q");
		SKeys.put("X", "X");
		SKeys.put("X", "X");
		SKeys.put("S", "S");
		SKeys.put("T", "T");
		SKeys.put("U", "U");
		SKeys.put("V", "V");
		SKeys.put("W", "W");
		SKeys.put("X", "X");
		SKeys.put("Y", "Y");
		SKeys.put("Z", "Z");

		SKeys.put("a", "a");
		SKeys.put("b", "b");
		SKeys.put("c", "c");
		SKeys.put("d", "d");
		SKeys.put("e", "e");
		SKeys.put("f", "f");
		SKeys.put("g", "g");
		SKeys.put("h", "h");
		SKeys.put("i", "i");
		SKeys.put("j", "j");
		SKeys.put("k", "k");
		SKeys.put("l", "l");
		SKeys.put("m", "m");
		SKeys.put("n", "n");
		SKeys.put("o", "o");
		SKeys.put("p", "p");
		SKeys.put("q", "q");
		SKeys.put("r", "r");
		SKeys.put("q", "q");
		SKeys.put("x", "x");
		SKeys.put("s", "s");
		SKeys.put("t", "t");
		SKeys.put("u", "u");
		SKeys.put("v", "v");
		SKeys.put("w", "w");
		SKeys.put("x", "x");
		SKeys.put("y", "y");
		SKeys.put("z", "z");

		Logs.Ulog(" SendKeys Loaded Successfully");
		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static WebElement Browser(String WhichObject) {

		try

		{
			String ORobj = (String) StoreTable.get(WhichObject);
			String OR[] = ORobj.split("~");
			TC.FindByProp = OR[0];
			TC.FindByPropVal = OR[1];

		}

		catch (Exception e) {

			Logs.Ulog("Object not found in the repository Please correct the object name column");

		}
		try {

			switch (TC.FindByProp) {
			case "xpath":
				return driver.findElement(By.xpath(TC.FindByPropVal));

			case "className":
				return driver.findElement(By.className(TC.FindByPropVal));

			case "cssSelector":
				return driver.findElement(By.cssSelector(TC.FindByPropVal));

			case "id":
				return driver.findElement(By.id(TC.FindByPropVal));

			case "linkText":
				return driver.findElement(By.linkText(TC.FindByPropVal));

			case "name":
				return driver.findElement(By.name(TC.FindByPropVal));
			case "partialLinkText":
				return driver.findElement(By.partialLinkText(TC.FindByPropVal));
			case "tagName":
				return driver.findElement(By.tagName(TC.FindByPropVal));
			default:
				TTlogs.debug("InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp);
			}

		} catch (Throwable t) {
			TTlogs.debug("Test Object Unable to find the Test Object "
					+ TC.FindByPropVal + " By Using Property " + TC.FindByProp);
		}
		return null;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static List<WebElement> BrowserElements(String WhichObject) {

		try

		{
			String ORobj = (String) StoreTable.get(WhichObject);
			String OR[] = ORobj.split("~");
			TC.FindByProp = OR[0];
			TC.FindByPropVal = OR[1];

		}

		catch (Exception e) {

			Logs.Ulog("Object not found in the repository Please correct the object name column");

		}

		try {

			if (TC.FindByProp.equals("xpath")) {

				return driver.findElements(By.xpath(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("className")) {

				return driver.findElements(By.className(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("cssSelector")) {

				return driver.findElements(By.cssSelector(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("id")) {

				return driver.findElements(By.id(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("linkText")) {

				return driver.findElements(By.linkText(TC.FindByPropVal));
			}
			if (TC.FindByProp.equals("name")) {

				return driver.findElements(By.name(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("partialLinkText")) {

				return driver
						.findElements(By.partialLinkText(TC.FindByPropVal));
			}

			if (TC.FindByProp.equals("tagName")) {

				return driver.findElements(By.tagName(TC.FindByPropVal));

			}

			if (TC.FindByProp.equals("")) {
				Logs.Ulog("InValid Property Selection  or Invalid Propertytype  or Nothing is selested on in the Object repository sheet Cell for "
						+ TC.FindByPropVal
						+ " By Using Property "
						+ TC.FindByProp);
			}

		} catch (Throwable t) {
			Logs.Ulog("Test Object Unable to find the Test Object By Find Elements "
					+ TC.TestObjects
					+ TC.FindByPropVal
					+ " By Using Property "
					+ TC.FindByProp);
		}

		return null;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ORViewElement(WebElement Obj) throws IOException,
			InterruptedException

	{
		Logs.Ulog("Starting ViewElement");
		if (StoreTable.get("ScrolltoViewElement").toString()
				.equalsIgnoreCase("YY")) {
			try {

				WrapsDriver wrappedElement = (WrapsDriver) Obj;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", Obj);

			} catch (Throwable T) {
				Logs.Ulog("Error while ViewElement" + T.getMessage());
			}

		}
		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static void Createfiles() {

		System.out.println("Testin");

		try {
			Date date = new Date();
			File myTemp = new File(TC.TempFolder + "/RSTemp");
			myTemp.mkdir();
			SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
			String formattedDate = sdf.format(date);

			File file = new File(System.getProperty("user.dir"));
			String myConfigfilepath = null;
			if (System.getProperty("excelFilePath") == null) {
				
				parentFolder = file.getParentFile();
				myConfigfilepath = parentFolder + "/Projects/GlobalSetup.xlsm";
			} else {
				String filePath = System.getProperty("excelFilePath");
				System.out.println("into jenkins path");
				System.out.println(filePath);
				myConfigfilepath = filePath+ "/Projects/GlobalSetup.xlsm";
			}
			File myConfig = new File(myConfigfilepath);

			try {
				FileUtils.copyFileToDirectory(myConfig, myTemp);
				File newFile = new File(myTemp + "/GlobalSetup.xlsm");
				File file2 = new File(newFile.getParentFile().getPath()
						+ "/GlobalSetup_" + formattedDate + ".xlsm");
				newFile.renameTo(file2);
				mySetupPathTemp = file2.getParentFile().getPath() + "/"
						+ file2.getName();
				System.out.println("mySetupPathTemp = " + mySetupPathTemp);
				GlobalSetupData = new ExcelObj(file2.getParentFile().getPath()
						+ "/" + file2.getName());
			} catch (Throwable T) {
				System.out
						.println("Error while creating GlobalSetup.xlsm file "
								+ T.getMessage());
				Logs.Ulog("Error while -  finding file GloabalSetup.xlsm in path "
						+ parentFolder + "/TestScripts/" + T.getMessage());
			}
			try {
				ProjectName = GlobalSetupData.getCellData(TC.GloablSetupSheet,
						TC.ConfigValuecol, 2);
				String mySetupfilepath=null;
				if(System.getProperty("excelFilePath")!=null){
					String filePath = System.getProperty("excelFilePath");
					 mySetupfilepath = filePath + "/Projects/" + ProjectName
						+ "/Setup_" + ProjectName + ".xlsm";
					 System.out.println("mySetupfilepath ="+mySetupfilepath);
					 myConfig = new File(mySetupfilepath);
				}
				else
				{
				  mySetupfilepath = parentFolder +
				"/Projects/"+ProjectName+"/Setup_"+ProjectName+".xlsm";
				myConfig = new File(mySetupfilepath);
				}
				FileUtils.copyFileToDirectory(myConfig, myTemp);
				if(System.getProperty("excelFilePath")!=null){
					String filePath = System.getProperty("excelFilePath");
					String filePathNew=filePath+ "/Projects/" + ProjectName;
					File newFile = new File(myTemp + "/Setup_" + ProjectName
							+ ".xlsm");
					File file2 = new File(filePathNew
							+ "/Setup_" + ProjectName + "_" + formattedDate
							+ ".xlsm");
					newFile.renameTo(file2);
					
					myConfigfilepathTemp = filePathNew + "/"
							+ file2.getName();
					System.out.println("inside jenkin config path");
					System.out.println("myConfigfilepathTemp ="+myConfigfilepathTemp);
					SetupData = new ExcelObj(filePathNew + "/"
							+ file2.getName());
				}
				else
				{
				File newFile = new File(myTemp + "/Setup_" + ProjectName
						+ ".xlsm");
				File file2 = new File(newFile.getParentFile().getPath()
						+ "/Setup_" + ProjectName + "_" + formattedDate
						+ ".xlsm");
				newFile.renameTo(file2);
				myConfigfilepathTemp = file2.getParentFile().getPath() + "\\"
						+ file2.getName();
				System.out.println("myConfigfilepathTemp ="+myConfigfilepathTemp);
				SetupData = new ExcelObj(file2.getParentFile().getPath() + "\\"
						+ file2.getName());
			}
			}

			catch (Throwable T) {
				System.out
						.println("Error while creating  Setup_Projectname.xlsm file,  verify - Please append the file name with proper project name "
								+ T.getMessage());
				Logs.Ulog("Error while -  finding file Setup_Projectname.xlsm in path "
						+ parentFolder
						+ "/"
						+ ProjectName
						+ "/"
						+ T.getMessage());
			}

			try {

				String Resourcefilename = SetupData.getCellData(TC.SetupSheet,
						TC.ConfigValuecol, 2); // SetupData.getCellData(TC.SelectProjectResourceSheet,
												// TC.ResourceFileNameCol , 2);
				
				if(System.getProperty("excelFilePath")!=null){
					String filePath = System.getProperty("excelFilePath");
					
				TC.TestDataPath = filePath + "/Projects/" + ProjectName
						+ "/TestData/";
				System.out.println("TC.TestDataPath = "+TC.TestDataPath);
				}
				else
				{
					 TC.TestDataPath = parentFolder + "/Projects/"+ProjectName +
							 "/TestData/";
				}
				String myResourcefilename=null;	
				if(System.getProperty("excelFilePath")!=null){
					String filePath = System.getProperty("excelFilePath");
					 myResourcefilename = filePath+ "/Projects/" + ProjectName
							+ "/" + Resourcefilename + ".xlsm";
				}
				else
				{
					 myResourcefilename = parentFolder +
							 "/Projects/"+ProjectName+ "/"+ Resourcefilename+".xlsm";
				}
				String myResourcefilenameTemp=null;
				File myResource = new File(myResourcefilename);
				if(System.getProperty("excelFilePath")!=null){
					String filePath = System.getProperty("excelFilePath");
	 myResourcefilenameTemp = filePath + "/Projects/" + ProjectName
			+ Resourcefilename + "_" + formattedDate + ".xlsm";
	 	System.out.println("myResourcefilenameTemp = "+myResourcefilenameTemp);
}
				else{
				  myResourcefilenameTemp = parentFolder+
				 "/Projects/"+ProjectName + Resourcefilename
				 +"_"+formattedDate+".xlsm";
				}
				// myResourcetemp = new File(myResourcefilenameTemp); 

				FileUtils.copyFileToDirectory(myResource, myTemp);
				File newFile = new File(myTemp + "/" + Resourcefilename
						+ ".xlsm");
				File file2 = new File(newFile.getParentFile().getPath() + "/"
						+ Resourcefilename + "_" + formattedDate + ".xlsm");
				newFile.renameTo(file2);
				myResourcetemp = newFile.getParentFile().getPath() + "/"
						+ file2.getName();
			
				System.out.println("myResourcetemp ="+myResourcetemp);
				Resourcedata = new ExcelObj(newFile.getParentFile().getPath()
						+ "/" + file2.getName());

			}

			catch (Throwable T) {
				System.out
						.println("Error while creating Resource .* filename.xlsm file  check the resource file name in the Step sheet configuration value ProjectResource value "
								+ T.getMessage());
				Logs.Ulog("Error while -  finding file Resource file name in 'Select resource Sheet' in work book Setup.xlsm in path "
						+ parentFolder + "/" + ProjectName + T.getMessage());
			}

		}

		catch (Throwable T) {
			Logs.Ulog("Error while Create files,  Please check your execlfiles availebal in the properfolder "
					+ T.getMessage());
		}

	}

	public static void CreateTestfiles() {

		System.out.println("Testing");

		try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
			String formattedDate = sdf.format(date);
			File myTemp = new File(TC.TempFolder + "/RSTemp");
			myTemp.mkdir();
			String myTestpath=null;
			if(System.getProperty("excelFilePath")!=null){
				String filePath = System.getProperty("excelFilePath");
				 myTestpath = filePath + "/Projects/"+ ProjectName
							+ "/TestScripts/" + ModuleDriver + ".xlsm";
			}
			else
			{
				  myTestpath = parentFolder+ "/Projects/"+ProjectName
							 +"/TestScripts/" +ModuleDriver+".xlsm";
			}
			
			System.out.println("inside directory");
			System.out.println(System.getProperty("user.dir"));
			File myConfig = new File(myTestpath);

			try {
				FileUtils.copyFileToDirectory(myConfig, myTemp);
				File newFile = new File(myTemp + "/" + ModuleDriver + ".xlsm");
				File file2 = new File(newFile.getParentFile().getPath() + "/"
						+ ModuleDriver + "_" + formattedDate + ".xlsm");
				newFile.renameTo(file2);
				Testdatatemp = file2.getParentFile().getPath() + "/"
						+ file2.getName();
				Testdata = new ExcelObj(file2.getParentFile().getPath() + "/"
						+ file2.getName());

			}

			catch (Throwable T) {
				System.out
						.println("Error while creating Module file .xlsm file "
								+ T.getMessage());
				Logs.Ulog("Error while -  finding file Module_ .* .xlsm in path "
						+ parentFolder
						+ "/"
						+ ProjectName
						+ "/TestScripts/"
						+ T.getMessage());
			}

		} catch (Throwable T) {
			Logs.Ulog("Error while Create files  Please check your Test module execl files availebal in the properfolder "
					+ T.getMessage());
		}

	}
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

}
