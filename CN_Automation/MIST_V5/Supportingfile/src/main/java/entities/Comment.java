package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Comment {
	String description;

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public Comment(String description) {
		this.description = description;
	}

	public Comment() {

	}

}
