package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TestCaseRun {

	
	String status;
	String comment;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public TestCaseRun(String status, String comment) {
		this.status = status;
		this.comment = comment;
	}
	
	public TestCaseRun() {
		// TODO Auto-generated constructor stub
	}

	
	
}
