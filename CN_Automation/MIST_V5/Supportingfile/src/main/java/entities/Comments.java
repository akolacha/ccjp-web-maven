package entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Comments {


	List<Comment> items;

	public List<Comment> getItems() {
		return items;
	}

	public void setItems(List<Comment> items) {
		this.items = items;
	}

	public Comments() {
		this.items = new ArrayList<>();
	}
	
	
}
